
// applies physics to the process who called it. Last only 1 frame
function hmap_apply(_hmap pointer hmap)
private
	i = 0, j = 0;
	advance_step, advance_distance;
	
	float inc_x_step,inc_y_step;
	
	float inc_x, inc_y;
	
	// int values for checking pixel collision
	int pos_x,pos_y;
	int prev_x,prev_y;

	float prev_hmap_x,prev_hmap_y; 

	bool x_blocked,y_blocked = false;
	bool inside_hardness = false;
	bool colliding;

	int collision_value;
begin
	if (father.hmap_gravity_affected)
		// apply Gravity	
		if ((father.hmap_inc_y + hmap_gravity) < hmap_max_vertical_speed)
			father.hmap_inc_y += hmap_gravity;
		else
			father.hmap_inc_y = hmap_max_vertical_speed;
		end
	end

	// limitamos posicion del personaje adentro del mapa de dureza
	if (father.x < 0)
		father.x = 0;
	end
	if (father.x >= hmap.resolution_x)
		father.x = hmap.resolution_x - 1;
	end
	if (father.y < 0)
		father.y = 0;
	end
	if (father.y >= hmap.resolution_y)
		father.y = hmap.resolution_y - 1;
	end

	
	// actualizamos posicion
	if (abs(father.hmap_x - father.x) > 1.0)
		father.hmap_x =  father.x;
	end
	if (abs(father.hmap_y - father.y) > 1.0)
		father.hmap_y =  father.y;
	end

	pos_x = round(father.hmap_x);
	pos_y = round(father.hmap_y);
		
	inc_x = (float) abs(round(father.hmap_inc_x));
	inc_y = (float) abs(round(father.hmap_inc_y));

	// calculamos avance pixel por pixel
	if (inc_x >= inc_y and inc_x>0)
		inc_x_step = 1; 
	else 
		inc_x_step = (inc_x/inc_y);
	end

	if (inc_y >= inc_x and inc_y>0)
		inc_y_step = 1 ; 
	else
		inc_y_step = (inc_y/inc_x);
	end
	
	// le damos el signo
	if (father.hmap_inc_x<0) inc_x_step *= -1; end
	if (father.hmap_inc_y<0) inc_y_step *= -1; end
	
	// avanzamos pixel por pixel calculando que no nos choquemos con nadina
	advance_step = 0;
	if (inc_x >= inc_y )
		advance_distance = inc_x;
	else
		advance_distance = inc_y;
	end

	hmap_collision_bottom = 0;
	hmap_collision_top = 0;
	hmap_collision_left = 0;
	hmap_collision_right = 0;

	hmap_x = father.hmap_x;
	hmap_y = father.hmap_y;

	while( advance_step < advance_distance)

		prev_x = pos_x;
		prev_y = pos_y;

		prev_hmap_x = hmap_x;
		prev_hmap_y = hmap_y;

		x_blocked = (inc_x_step == 0.0)? true:false;
		y_blocked = (inc_y_step == 0.0)? true:false;
		
		// avanzamos de a un pixel aca
		if (!x_blocked)
			hmap_x += inc_x_step;
		end
		if(!y_blocked)
			hmap_y += inc_y_step;
		end

		// actualiza posicion despues de mover
		pos_x = round(hmap_x);
		pos_y = round(hmap_y);

		// check collision
		if (prev_y < pos_y)			 
			hmap_collision_bottom = 0;			
		end
		if (prev_y > pos_y)
			hmap_collision_top = 0;			
		end
		if (prev_x > pos_x)
			hmap_collision_left = 0;			
		end
		if (prev_x < pos_x)
			hmap_collision_right = 0;			
		end

		// arreglo de dureza (mueve el proceso afuera de la dureza)
		Repeat				
			colliding = false;

			if (prev_y < pos_y)
				if (collision_value = hmap_collision_bottom(father, round(hmap_x), round(hmap_y), hmap) > 0)
					if (father.fluidity == 0.0)
						hmap_y -= 1.0;						
						colliding = true;
						y_blocked = true;
					else
						hmap_y = prev_hmap_y + ((hmap_y - prev_hmap_y)*father.fluidity);						
					end
					hmap_collision_bottom = collision_value;
				end
			end

			if (prev_y > pos_y)
				if(collision_value = hmap_collision_top(father, round(hmap_x), round(hmap_y), hmap) > 0)
					if (father.fluidity == 0.0)
						hmap_y += 1.0;
						colliding = true;
						y_blocked = true;
					else
						hmap_y = prev_hmap_y + ((hmap_y - prev_hmap_y)*father.fluidity);
					end
					hmap_collision_top = collision_value;
				end
			end

			if (prev_x > pos_x)
				if(collision_value = hmap_collision_left(father, round(hmap_x), round(hmap_y), hmap) > 0)
					if (father.fluidity == 0.0)
						hmap_x += 1.0;
						colliding = true;
						x_blocked = true;	
					else
						hmap_x = prev_hmap_x + ((hmap_x - prev_hmap_x)*father.fluidity);
					end
					hmap_collision_left = collision_value;
				end
			end

			if ( prev_x < pos_x)
				if (collision_value = hmap_collision_right(father, round(hmap_x), round(hmap_y), hmap) > 0)
					if (father.fluidity == 0.0)
						hmap_x -= 1.0;
						colliding = true;
						x_blocked = true;	
					else
						hmap_x = prev_hmap_x + ((hmap_x - prev_hmap_x)*father.fluidity);
					end
					hmap_collision_right = collision_value;
				end
			end

			

		Until(colliding == false)			

		// actualiza posicion despues del arreglo de dureza
		pos_y = round(hmap_y);
		pos_x = round(hmap_x);
		

		if (x_blocked and y_blocked)
			break;
		end
			

		advance_step ++;
	end

	father.hmap_x = hmap_x;
	father.hmap_y = hmap_y;

	// actualizamos banderas de colision
	father.hmap_collision_bottom = hmap_collision_bottom;
	father.hmap_collision_top = hmap_collision_top;
	father.hmap_collision_left = hmap_collision_left;
	father.hmap_collision_right = hmap_collision_right;
	
	
	// actualizamos posicion del padre
	father.x = round(father.hmap_x);
	father.y = round(father.hmap_y);
	
	// rebote
	if ((father.hmap_collision_bottom>0 or father.hmap_collision_top>0))
		father.hmap_inc_y = (father.hmap_inc_y * father.hmap_bounce_rate) * -1; // bouncing		
	end
	if ((father.hmap_collision_left>0 or father.hmap_collision_right>0))	
		father.hmap_inc_x = (father.hmap_inc_x * father.hmap_bounce_rate) * -1; // bouncing
	end
	
end

