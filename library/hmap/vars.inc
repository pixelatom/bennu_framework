
global
	// configuration of the global physics that affects all procceses
	float hmap_gravity = 1;
	float hmap_max_vertical_speed = 12;
	hmap_array_mode = false; // PROTOTYPE FEATURE. NOT TESTED
end

local
	// physics vars of each proccees
	
	// hardness map collision box
	hmap_box_height = 0;
	hmap_box_width = 0;
	
	// process position
	float hmap_x,hmap_y;
	
	// process speed
	float hmap_inc_x = 0; //velocidad horizontal
	float hmap_inc_y = 0; //velocidad vertical
	
	// saves the las color checked on each boundary
	int hmap_collision_bottom = 0; //colision abajo
	int hmap_collision_top = 0; //colision arriba
	int hmap_collision_left = 0; //colision izquierda
	int hmap_collision_right = 0; //colision derecha		
	
	// physics properties
	float hmap_bounce_rate = 0; // how much bounces percentage (from zero to one, ex: 0.2 = 20%)
	
	// affected by gravity?
	bool hmap_gravity_affected = true;

	float fluidity = 0.0;
end

type _hmap
	resolution_x;
	resolution_y;
	width = 0;
	height = 0;
	int pointer map = null; // map array	
	file;
	graph;
	float ratio = 1;
end