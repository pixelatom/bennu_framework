
function hmap_get_pixel(_hmap pointer hmap,int x, int y)
begin
	x = (x * hmap.ratio);
	y = (y * hmap.ratio);
	if ( x < hmap.width and  y < hmap.height and x >= 0 and y >= 0 )
		if (hmap_array_mode)
			return hmap.map[((y * hmap.width) + x )];
		else
			return map_get_pixel(hmap.file,hmap.graph,x,y);
		end
	else
		return 0;
	end
end



function hmap_collision_bottom(process_id, float fpos_x, float fpos_y,_hmap pointer hmap)

begin
	
	y = fpos_y + (process_id.hmap_box_height/2);
	for (x = fpos_x - (process_id.hmap_box_width / 4) -1; x <= fpos_x + (process_id.hmap_box_width / 4) +1; x += 1/hmap.ratio)
			
		hmap_collision_bottom = hmap_get_pixel(hmap,x,y);
		if (hmap_collision_bottom>0)
			return hmap_collision_bottom;
		end
	end
	return 0;
end

function hmap_collision_top(process_id, float fpos_x, float fpos_y,_hmap pointer hmap)

begin
		y = fpos_y - (process_id.hmap_box_height/2);
	for (x = fpos_x - (process_id.hmap_box_width / 4) -1 ; x <= fpos_x + (process_id.hmap_box_width / 4) +1 ; x += 1/hmap.ratio)
			
		hmap_collision_top = hmap_get_pixel(hmap,x,y);
		if (hmap_collision_top>0)
			return hmap_collision_top;
		end
	end
	return 0;
end

function hmap_collision_left(process_id, float fpos_x, float fpos_y,_hmap pointer hmap)
begin
	
	x = fpos_x - (process_id.hmap_box_width/2);
	for (y = fpos_y - (process_id.hmap_box_height / 4) -1 ; y <= fpos_y + (process_id.hmap_box_height / 4)+1 ; y += 1/hmap.ratio)
			
		hmap_collision_left = hmap_get_pixel(hmap,x,y);
		if (hmap_collision_left>0)
			return hmap_collision_left;
		end
	end
	return 0;
end

function hmap_collision_right(process_id, float fpos_x, float fpos_y,_hmap pointer hmap)
begin
	x = fpos_x + (process_id.hmap_box_width/2);
	for (y = fpos_y - (process_id.hmap_box_height / 4) -1;  y <= fpos_y + (process_id.hmap_box_height / 4)+1; y += 1/hmap.ratio)
		hmap_collision_right = hmap_get_pixel(hmap,x,y);
		if (hmap_collision_right>0)
			return hmap_collision_right;
		end
	end
	return 0;
end

function hmap_collision(process_id,_hmap pointer hmap)
begin
	return 	hmap_collision_bottom(process_id, process_id.x, process_id.y, hmap) or
			hmap_collision_top(process_id, process_id.x, process_id.y, hmap) or
			hmap_collision_left(process_id, process_id.x, process_id.y, hmap) or 
			hmap_collision_right(process_id, process_id.x, process_id.y, hmap);
end

function hmap_inside_hardness(process_id,_hmap pointer hmap)
begin
	return 	hmap_collision_bottom(process_id, process_id.x, process_id.y, hmap) and
			hmap_collision_top(process_id, process_id.x, process_id.y, hmap) and
			hmap_collision_left(process_id, process_id.x, process_id.y, hmap) and 
			hmap_collision_right(process_id, process_id.x, process_id.y, hmap);
end