
/**
 * init an hmap with a graph information
 */
function hmap_init(_hmap pointer hmap, int file, int graph, float ratio)
private
	i = 0, j = 0;	
begin

	hmap.height = (map_info(file,graph,G_HEIGHT)*ratio); 
	hmap.width = (map_info(file,graph,G_WIDTH)*ratio) ;

	hmap.resolution_x = map_info(file,graph,G_WIDTH);
	hmap.resolution_y = map_info(file,graph,G_HEIGHT);

	hmap.file = file;
	hmap.graph = graph;
	hmap.ratio = ratio;

	if (hmap_array_mode)
		hmap.map = alloc(hmap.height * hmap.width * sizeof(int));
		memseti ( hmap.map , 0 , hmap.height * hmap.width);
	else
		hmap.file = 0;
		hmap.graph = map_new(hmap.width, hmap.height, map_info(file,graph,G_DEPTH), B_CLEAR );
	end

	x = 0;
	for (i = 0; i < hmap.width; i ++)
		x += 1/(ratio);
		y = 0;
		for (j = 0; j < hmap.height; j++)
			y += 1/(ratio);
			if (hmap_array_mode)
				hmap.map[((j * hmap.width) + i )] =  map_get_pixel(file,graph,x-1,y-1);
			else
				map_put_pixel(0,hmap.graph,i,j,map_get_pixel(file,graph,x-1,y-1));
				
			end
		end		
	end

end

function hmap_free(_hmap pointer hmap)
begin
	if (hmap_array_mode)
		mem_free(hmap.map);
		hmap.map = null;
	else
		map_unload (0,hmap.graph);
	end
end