#ifndef __SCREEN_INFO
#define __SCREEN_INFO

import "mod_map";

function get_color_depth()
begin
	return graphic_info ( 0,BACKGROUND , G_DEPTH );
end

function get_screen_width()
begin
	return graphic_info ( 0,BACKGROUND , G_width );
	//return (scale_mode!=SCALE_NONE)?graphic_info ( 0,BACKGROUND , G_width )/2:graphic_info ( 0,BACKGROUND , G_width );
	
end

function get_screen_height()
begin
	return graphic_info ( 0,BACKGROUND , G_height );
	//return (scale_mode!=SCALE_NONE)?graphic_info ( 0,BACKGROUND , G_height )/2:graphic_info ( 0,BACKGROUND , G_height );
end


function get_current_color_depth()
begin
	return graphic_info ( 0,BACKGROUND , G_DEPTH );
end


#endif