include "../library/lists.inc";

#ifndef __DICTIONARY_LIB
#define __DICTIONARY_LIB

type _dictionary
	_string_list keys;
	_int_list values;
end


function dictionary_exists(_dictionary pointer dictionary, string key)
private
	i;
begin
	//say("dictionary index for "+key+ ":"+dictionary_get_index(dictionary, key));
	return (dictionary_get_index(dictionary, key) != -1);
end

function dictionary_get_index(_dictionary pointer dictionary, string key)
private
	i;
begin
	for (i=0; i < int_list_count(&(dictionary.values)); i++)
		if (string_list_get(&(dictionary.keys),i) == key)
			return i;
		end
	end
	
	return -1;
end

function dictionary_set(_dictionary pointer dictionary, string key, int value)
private
	i;
begin
	if ((i = dictionary_get_index(dictionary,key)) != -1)  
		return int_list_set(&(dictionary.values), i, value);
	else
		string_list_add(&(dictionary.keys), key);
		return int_list_add(&(dictionary.values), value);
	end
end

function dictionary_get(_dictionary pointer dictionary, string key)
private
	i;
begin
	//say(key + " index: "+ dictionary_get_index(dictionary, key));
	if ((i = dictionary_get_index(dictionary, key)) == -1)
		return false;
	end

	return int_list_get(&(dictionary.values), i);
end

function dictionary_clear(_dictionary pointer dictionary)
begin
	string_list_clear(&(dictionary.keys));
	return int_list_clear(&(dictionary.values));
end

function dictionary_remove(_dictionary pointer dictionary, string key)
private
	i;
begin
	if ((i = dictionary_get_index(dictionary, key)) == -1)
		return false;
	end

	string_list_remove(&(dictionary.keys),i);
	return int_list_remove(&(dictionary.values),i);
end

function dictionary_count(_dictionary pointer dictionary)
begin
	return int_list_count(&(dictionary.values));
end

#endif