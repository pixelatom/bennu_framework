/**
 * Control input management library
 *
 */

include "../library/events.inc";

#ifndef __INPUT_LIB
#define __INPUT_LIB

import "mod_key"; 
import "mod_joy"; 
import "mod_mem"; 
import "mod_string";
import "mod_file";

// default control action constants
const
INPUT_UI_OK = 1;
INPUT_UI_CANCEL = 2;
INPUT_UI_LEFT = 3;
INPUT_UI_RIGHT = 4;
INPUT_UI_UP = 5;
INPUT_UI_DOWN = 6;
end

type input_buttonmap
	int control = 0; // the control action we want to check
	int player = 0; // the player number (when multiplayer)
	int value = 0; // the key id in the device (keyboard key, joy button, etc)	
	int device_id = -1; // when more than one device conected (joysticks)
	int axisnumber = -1; // when joystick
	int hatnumber = -1; // when joystick
end

global
// input_defaultjoymap[5] = 9,8,JOY_HAT_LEFT,JOY_HAT_RIGHT,JOY_HAT_UP,JOY_HAT_DOWN;
int input_mappings_keyboard_count = 0;
input_buttonmap pointer input_mappings_keyboard; // list of mapped buttons to the keyboard
int input_mappings_joystick_count = 0;
input_buttonmap pointer input_mappings_joystick; // list of mapped buttons to the joysticks

int pointer input_disabled = null;
input_scancodes[98] = 'Esc','1','2','3','4','5','6','7','8','9','0','-','+','Back','Tab','Q','W','E','R','T','Y','U','I','O','P','[',']','Enter','Ctrl','A','S','D','F','G','H','J','K','L',';',"'",'W','LShift','\\','Z','X','C','V','B','N','M',',','.','/','Rshift','PrScr','Alt',' ','Caps','F1','F2','F3','F4','F5','F6','F7','F8','F9','F10','NumLock','ScrLock','Home','Up','PgUp','-','Left','5','Right','+','End','Down','PgDown','Ins','Del','None','None','None','F11','F12','Less','=','>','*','rAlt','rCtrl','lAlt','rCrl','Menu','Windows';
end


function input_disable(player)
private
	int i;
	elements = 0;
	
begin
	if (input_disabled!=null)
		elements = (sizeof(input_disabled)/sizeof(int));
		for(i=0; i<elements; i++)
			if (input_disabled[i] == player)
				return;
			end
		end
	end
	
	input_disabled = realloc(input_disabled,(elements+1)*sizeof(int));
	elements++;
	input_disabled[elements - 1] = player;
end

function input_enable(player)
private
	int i;
	elements = 0;
	int pointer new_input_disabled = null;
begin
	if (input_disabled != null)		
		elements = (sizeof(input_disabled)/sizeof(int));
		for(i=0; i<elements; i++)
			if (input_disabled[i] != player)
				if (new_input_disabled == null)
					new_input_disabled = alloc(sizeof(int));
				else
					new_input_disabled = realloc(new_input_disabled,(sizeof(new_input_disabled)/sizeof(int)) + 1);
				end
				input_disabled[(sizeof(new_input_disabled)/sizeof(int)) - 1] = input_disabled[i];
			end
		end
		
		free(input_disabled);
		input_disabled = new_input_disabled;
	end
end

function input_is_enabled(player)
private
	int i;
	elements = 0;
begin
	if (input_disabled==null)
		return true;
	end
	
	elements = (sizeof(input_disabled)/sizeof(int));
	
	for(i=0; i<elements; i++)
		if (input_disabled[i] == player or input_disabled[i] == 0)
			return false;
		end
	end	
	
	return true;
end

/**
 * Removes all controls associated to an action
 */
function input_clear_button(player, button)
private
int i;
input_buttonmap pointer mappedcontrol_;

input_buttonmap pointer new_input_mappings_keyboard;
int new_input_mappings_keyboard_count = 0;

input_buttonmap pointer new_input_mappings_joystick;
int new_input_mappings_joystick_count = 0;

int _size = 0;
begin
	if (input_mappings_keyboard != NULL)
		// busca una tecla configurada para el control
		for (i = 0; i < input_mappings_keyboard_count;i++) 
			mappedcontrol_ = input_mappings_keyboard[i];
			if (mappedcontrol_.control == button and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
				continue; // si es del player y el boton indicado no lo pasamos al nuevo puntero				
			else	
				// calcula el nuevo tamaño
				if ((new_input_mappings_keyboard) == NULL)
					_size = sizeof(input_buttonmap);
				else
					_size = (new_input_mappings_keyboard_count+1) *  sizeof(input_buttonmap);
				end
				
				// redimencionar tamaño del puntero
				new_input_mappings_keyboard = realloc (new_input_mappings_keyboard, _size);
				
				new_input_mappings_keyboard_count++;
				
				// setear el nuevo elemento	
				new_input_mappings_keyboard[new_input_mappings_keyboard_count - 1].control = mappedcontrol_.control;
				new_input_mappings_keyboard[new_input_mappings_keyboard_count - 1].player = mappedcontrol_.player;
				new_input_mappings_keyboard[new_input_mappings_keyboard_count - 1].value = mappedcontrol_.value;				
			end
		end	
		
		free(input_mappings_keyboard); // libera mem del puntero viejo
		
		input_mappings_keyboard = new_input_mappings_keyboard;
		input_mappings_keyboard_count = new_input_mappings_keyboard_count;		
	end
	
	_size = 0;
	
	if (input_mappings_joystick != NULL)
		// busca una tecla configurada para el control
		for (i = 0; i < input_mappings_joystick_count;i++) 
			mappedcontrol_ = input_mappings_joystick[i];
			if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
				((player != 0 and mappedcontrol_.player == player) or (player == 0)))
				
				continue; // si es del player y el boton indicado no lo pasamos al nuevo puntero				
			else	
				// calcula el nuevo tamaño
				if ((new_input_mappings_joystick) == NULL)
					_size = sizeof(input_buttonmap);
				else
					_size = (new_input_mappings_joystick_count+1) *  sizeof(input_buttonmap);
				end
				
				// redimencionar tamaño del puntero
				new_input_mappings_joystick = realloc (new_input_mappings_joystick, _size);
				
				new_input_mappings_joystick_count++;
				
				// setear el nuevo elemento	
				new_input_mappings_joystick[new_input_mappings_joystick_count - 1].control = mappedcontrol_.control;
				new_input_mappings_joystick[new_input_mappings_joystick_count - 1].player = mappedcontrol_.player;
				new_input_mappings_joystick[new_input_mappings_joystick_count - 1].value = mappedcontrol_.value;
				new_input_mappings_joystick[new_input_mappings_joystick_count - 1].device_id = mappedcontrol_.device_id;
				new_input_mappings_joystick[new_input_mappings_joystick_count - 1].axisnumber = mappedcontrol_.axisnumber;
				new_input_mappings_joystick[new_input_mappings_joystick_count - 1].hatnumber = mappedcontrol_.hatnumber;
			end
		end	
		
		free(input_mappings_joystick); // libera mem del puntero viejo
		
		input_mappings_joystick = new_input_mappings_joystick;
		input_mappings_joystick_count = new_input_mappings_joystick_count;		
	end
	
	
	return;
end

/**
 * Maps a keyboard scancode to a game control action.
 */
function input_map_keyboard(int control,int player, int scancode)
private
int i;
input_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((input_mappings_keyboard) == NULL)
		_size = sizeof(input_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < input_mappings_keyboard_count;i++) 
			mappedcontrol_ = input_mappings_keyboard[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.value == scancode)			
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (input_mappings_keyboard_count+1) *  sizeof(input_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	input_mappings_keyboard = realloc (input_mappings_keyboard, _size);
	
	input_mappings_keyboard_count++;
	
	// setear el nuevo elemento	
	input_mappings_keyboard[i].control = control;
	input_mappings_keyboard[i].player = player;
	input_mappings_keyboard[i].value = scancode;
end
/**
 * Maps a joystick axis to an action
 */
function input_map_joy_hat(int control, int player, int joystick_id, int hatnumber, int value)
private
int i;
input_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((input_mappings_joystick) == NULL)
		_size = sizeof(input_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < input_mappings_joystick_count;i++) 
			mappedcontrol_ = input_mappings_joystick[i];
			if (mappedcontrol_.control == (control  - 10000)  and 
				mappedcontrol_.player == player and 
				mappedcontrol_.hatnumber == hatnumber and
				mappedcontrol_.value == value )
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (input_mappings_joystick_count+1) *  sizeof(input_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	input_mappings_joystick = realloc (input_mappings_joystick, _size);
	
	input_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	input_mappings_joystick[i].control = (control  - 10000);
	input_mappings_joystick[i].device_id = joystick_id;
	input_mappings_joystick[i].player = player;
	input_mappings_joystick[i].hatnumber = hatnumber;
	input_mappings_joystick[i].axisnumber = -1;
	input_mappings_joystick[i].value = value ;
end
/**
 * Maps a joystick axis to an action
 */
function input_map_joy_axis(int control, int player, int joystick_id, int axisnumber, int value)
private
int i;
input_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((input_mappings_joystick) == NULL)
		_size = sizeof(input_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < input_mappings_joystick_count;i++) 
			mappedcontrol_ = input_mappings_joystick[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.axisnumber == axisnumber and
				mappedcontrol_.value == value)
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (input_mappings_joystick_count+1) *  sizeof(input_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	input_mappings_joystick = realloc (input_mappings_joystick, _size);
	
	input_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	input_mappings_joystick[i].control = control;
	input_mappings_joystick[i].device_id = joystick_id;
	input_mappings_joystick[i].player = player;
	input_mappings_joystick[i].axisnumber = axisnumber;
	input_mappings_joystick[i].hatnumber = -1;
	input_mappings_joystick[i].value = value;
end

/**
 * Maps a joystick button to a game control action.
 */
function input_map_joystick(int control, int player, int joystick_id, int button)
private
int i;
input_buttonmap pointer mappedcontrol_;
int _size = 0;
begin
	// calcular el tamaño nuevo del puntero.
	if ((input_mappings_joystick) == NULL)
		_size = sizeof(input_buttonmap);
	else
		
		// buscar que no exista already
		for (i = 0; i < input_mappings_joystick_count;i++) 
			mappedcontrol_ = input_mappings_joystick[i];
			if (mappedcontrol_.control == control and 
				mappedcontrol_.player == player and 
				mappedcontrol_.device_id == joystick_id and 
				mappedcontrol_.value == button)
				
				return; // duplicated
			end
		end		
		// calcula el nuevo tamaño
		_size = (input_mappings_joystick_count+1) *  sizeof(input_buttonmap);
	end
	
	// redimencionar tamaño del puntero
	input_mappings_joystick = realloc (input_mappings_joystick, _size);
	
	input_mappings_joystick_count++;
	
	// setear el nuevo elemento	
	input_mappings_joystick[i].control = control;
	input_mappings_joystick[i].device_id = joystick_id;
	input_mappings_joystick[i].player = player;
	input_mappings_joystick[i].value = button;
	input_mappings_joystick[i].axisnumber = -1;
	input_mappings_joystick[i].hatnumber = -1;
end


 
/**
 * Check if a button is pressed via the keyboard handler
 */
function input_button_checkkeyb(player, button)
private
int i;
input_buttonmap pointer mappedcontrol_;
begin
	// busca una tecla configurada para el control
	for (i = 0; i < input_mappings_keyboard_count;i++) 
		mappedcontrol_ = input_mappings_keyboard[i];
		if (mappedcontrol_.control == button and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)))			
			
			if (key(mappedcontrol_.value))
				return true;
			end
		end
	end
	
	return false;
end

/**
 * returns true if a button on any device binded to an action is pressed. 
 * if the device button is an axis it will return the value of the axis
 */
function input_button_checkjoy(player, button)
private
int i;
input_buttonmap pointer mappedcontrol_;
value;
begin
	// busca una tecla configurada para el control
	for (i = 0; i < input_mappings_joystick_count;i++) 
		mappedcontrol_ = input_mappings_joystick[i];
		
		if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
			((player != 0 and mappedcontrol_.player == player) or (player == 0)) and mappedcontrol_.device_id > -1)
			
			// check joystick
			
			
			if(mappedcontrol_.axisnumber > -1 and mappedcontrol_.control == button )
				if (mappedcontrol_.value > 0)
					if((value = joy_getaxis(mappedcontrol_.device_id,mappedcontrol_.axisnumber)) > mappedcontrol_.value)
						return value;
					end
				else
					if((value = joy_getaxis(mappedcontrol_.device_id,mappedcontrol_.axisnumber)) < mappedcontrol_.value)
						return value;
					end
				end
			end
			if(mappedcontrol_.hatnumber > -1 and mappedcontrol_.control == (button  - 10000))
				// HAT check
				if (joy_gethat(mappedcontrol_.device_id ,0) == mappedcontrol_.value)
					return true;
				end
			end
			if (joy_getbutton(mappedcontrol_.device_id ,mappedcontrol_.value) and mappedcontrol_.control == button)
				return true;
			end
			
			// •  •  •  •  •  •  •  • 
			
		end
	end
	
	return false;
end

/**
 * programatically emulates pressing a button
 */
function input_press(player, button)
private
	event e;
begin
	e = trigger("input press");
	event_set_arg(e,"player",player);
	event_set_arg(e,"button",button);
	event_set_arg(e,"value",true);
	//say("input_press " + player + " button " + button);
	return e;
end

/**
 * programatically press an action button for a player.
 * value is used for analog values
 */
function input_press_value(player, button, value)
private
	event e;
begin
	e = input_press(player, button);
	event_set_arg(e,"value", value);
	return e;
end

/**
 * Check for if a control button is pressed in any controller device
 */
function input(player, button)
private
	value;	
	event e;
begin
	if (!input_is_enabled(player))
		_input_release_pressed(player,button);
		return false;
	end

	// check keyboard
	if (input_button_checkkeyb(player, button))
		_input_set_pressed(player,button);
		return true;
	end
	// check joystick
    if ((value = input_button_checkjoy(player, button)) != false )
    	_input_set_pressed(player,button);
		return value;
	end

	// check programatic events 
	while (e=get_id(type event)) 
		if (e.__event_name != "input press") 
			continue; 
		end
		
		if (event_get_arg(e,"player") == player and 
			event_get_arg(e,"button") == button)

			//say("input pressed player " +player +" button "+button  );
			//say("input pressed player " +player +" button "+button +" value "+ event_get_arg(e,"value") );

			_input_set_pressed(player,button);
			return event_get_arg(e,"value");
		end
	end

	_input_release_pressed(player,button);
	return false;
end

/**
 * check for a control press event.
 * returns true only when the players presses the button
 * false if the player is still pressing it.
 * TODO: cuando se llama input_on() dos veces en el mismo frame empieza a retornal false
 * en un mismo frame deberia devolver siempre lo mismo
 */
function input_on(player,button)
begin
	if (_input_still_pressed(player,button) and input(player,button))
		return false;
	end	
	return input(player,button);
end



type _input_pressed_button
	int player;
	int button;
end

global
	_input_pressed_button pointer _input_pressed_buttons;
	_input_pressed_buttons_count = 0;
end

/**
 * returns true if the button was in pressed status in the last frame
 * TODO:
 * Hacer un pressed_last_frame
 * necesitamos un proceso que corra al final del frame, para que copie
 * el array de pressed a un array pressed last frame
 * ese proceso deberia ser levantado por control si no existe
 */
function _input_still_pressed(player,button)
private
	_input_pressed_button pressed;
	_size;
	i;
begin	
	if (_input_pressed_buttons != NULL)		
		for (i = 0; i < _input_pressed_buttons_count;i++) 
			pressed = _input_pressed_buttons[i];
			if (pressed.button == button and 
				pressed.player == player)				
				return true;
			end
		end		
	end
	return false;
end

/**
 * Agrega el control al registro de botones que estan siendo apretados
 */
function _input_set_pressed(player,button)
private
	_input_pressed_button pressed;
	_size;
	i;
begin

	// already in list
	if (_input_still_pressed(player,button))
		return;
	end

	// calcular el tamaño nuevo del puntero.
	_size = (_input_pressed_buttons_count+1) *  sizeof(_input_pressed_button);
	
	
	// redimencionar tamaño del puntero
	_input_pressed_buttons = realloc (_input_pressed_buttons, _size);	
	_input_pressed_buttons_count++;
	
	// setear el nuevo elemento	
	_input_pressed_buttons[_input_pressed_buttons_count - 1].button = button;
	_input_pressed_buttons[_input_pressed_buttons_count - 1].player = player;
	return true;
end
/**
 * elimina el control del registro de botones que esta siendo apretados
 */
function _input_release_pressed(player,button)
private
i;
_input_pressed_button pressed;
_input_pressed_button pointer new_control_pressed_buttons;
new_control_pressed_buttons_count=0;
_size = 0;
begin
	// if not in list, nothing to do
	if (!_input_still_pressed(player,button))
		return;
	end

	if (_input_pressed_buttons != NULL)
		// busca una tecla configurada para el control
		for (i = 0; i < _input_pressed_buttons_count;i++) 
			pressed = _input_pressed_buttons[i];
			if (pressed.button == button and pressed.player == player)				
				continue; // si es del player y el boton indicado no lo pasamos al nuevo puntero				
			else	
				// calcula el nuevo tamaño				
				_size = (new_control_pressed_buttons_count+1) *  sizeof(_input_pressed_button);				
				
				// redimencionar tamaño del puntero
				new_control_pressed_buttons = realloc (new_control_pressed_buttons, _size);
				
				new_control_pressed_buttons_count++;
				
				// setear el nuevo elemento					
				new_control_pressed_buttons[new_control_pressed_buttons_count - 1].player = pressed.player;
				new_control_pressed_buttons[new_control_pressed_buttons_count - 1].button = pressed.button;				
			end
		end	
		
		free(_input_pressed_buttons); // libera mem del puntero viejo
		
		_input_pressed_buttons = new_control_pressed_buttons;
		_input_pressed_buttons_count = new_control_pressed_buttons_count;		
	end	
end

/**
 * Check the button is pressed and then released in the same frame 
 */ 
function input_wait_release(player, button)
private
    value, return_value = false;	
begin
    while((value = input(player, button)) != false)		
		return_value = value;
        frame; // limpiamos buffer de teclas 
    end

    return return_value;
end


/**
 * returns true if any input method is registering a pressed button
 * even if it is not mapped to an action
 */
function input_is_any_pressed()
private 
	int i,j;
begin
	if (scan_code!= 0)
		return true;
	end

	for (i=0;i< joy_numjoysticks();i++)
		if(joy_getbutton(i,j))
			return true;
		end
		for (j=0;j < joy_numaxes (i); j++)
			if(joy_getaxis(i,j)<-20000);
				return true;
			end                
			if(joy_getaxis(i,j)>20000);
				return true;
			end
		end
		for (j=0;j < joy_numhats(i); j++)
			if(JOY_GETHAT(i,j))
				return true;
			end
		end
	end

	return false;
end

/**
 * Waits for an user to press a button and assign it to an action
 * returns:
 * true if a button was assigned
 * false if the button was cleared
 */
function input_listen_assign(player, button)
private
    i,j,value;
    bool assigned = false;
begin
    // esperamos a que el usuario deje de apretar un boton
	while (input_is_any_pressed())
		frame;
	end
    
    loop
        if (assigned) break; end
        
        frame; // aca en teoria se escucha el input
        
        // si se aprieta una tecla la definimos para la accion
        if (scan_code!= 0)
            input_map_keyboard(button,player, scan_code);            
            assigned = true;
            break;
        end
        
		// recorre todos los joysticks enchufados
        for (i=0;i< joy_numjoysticks();i++)
        
            if (assigned) break; end
            
			// checkea todos los botones del gamepad
            for (j=0;j < joy_numbuttons (i); j++)
                if(joy_getbutton(i,j))
                    input_map_joystick(button, player, i, j);
                    assigned =  true;
                    break;
                end
            end
            
            if (assigned) break; end
            
			// lee axis
            for (j=0;j < joy_numaxes (i); j++)
                if(joy_getaxis(i,j)<-20000);
                    input_map_joy_axis(button,  player, i, j, -20000);
                    assigned =  true;
                    break;
                end                
                if(joy_getaxis(i,j)>20000);
                    input_map_joy_axis(button,  player, i, j, 20000);
                    assigned =  true;
                    break;
                end
            end
            
            if (assigned) break; end
            
			// lee hats
            for (j=0;j < joy_numhats(i); j++)
                if(value=JOY_GETHAT(i,j))
                    input_map_joy_hat(button, player, i, j, value);
                    assigned =  true;
                    break;
                end
            end
        end
    end
    frame;
end

/**
 * Saves the current buttons configuration
 */
function input_save_config(STRING filepath)
Private
    handle;   // handle for the loaded file 
    int i;
    input_buttonmap mappedcontrol_;
Begin
    handle=fopen(filepath,O_WRITE); // opens the file in writing mode
    
    fwrite(handle,input_mappings_keyboard_count); // writes the quantity of keyb keys assigned
    
    // loop the list of keyb keys assigned
    for (i = 0; i < input_mappings_keyboard_count;i++) 
        mappedcontrol_ = input_mappings_keyboard[i];     
        fwrite(handle,mappedcontrol_); // writes each one of the keys assigned
    end
    
    fwrite(handle,input_mappings_joystick_count); // writes the quantity of joy buttons assigned
    
    // loop the list of joy buttons assigned
    for (i = 0; i < input_mappings_joystick_count;i++) 
        mappedcontrol_ = input_mappings_joystick[i];     
        fwrite(handle,mappedcontrol_); // writes each one of the keys assigned
    end
    
    fclose(handle);                 // zipping up after business is done
end

/**
 * returns true if there is anything configured for this player
 */
function input_player_has_config(player)
private
	int i;
begin
	for (i = 0; i < input_mappings_keyboard_count;i++)
		if (input_mappings_keyboard[i].player == player)
			return true;
		end
	end
	for (i = 0; i < input_mappings_joystick_count;i++)
		if (input_mappings_joystick[i].player == player)
			return true;
		end
	end
	return false;
end

function input_joystick_is_used(joy_id)
private
	int i;
begin
	for (i = 0; i < input_mappings_joystick_count;i++)
		if (input_mappings_joystick[i].device_id == joy_id)
			return true;
		end
	end

	return false;
end


/**
 * Loads buttons configuration from file
 */
function input_load_config(STRING filepath);
Private
    handle;   // handle for the loaded file 
	int i;
	
	_size;
	input_buttonmap mappedcontrol_;
Begin
    handle=fopen(filepath,O_READ); // opens the file in writing mode
	
	// keyboard
    fread(handle,input_mappings_keyboard_count); // reads the quantity of keyb keys assigned
	if (input_mappings_keyboard_count == 0)
		free(input_mappings_keyboard); // libera mem del puntero viejo
		input_mappings_keyboard = null;
	else
		// redimencionar tamaño del puntero
		_size = (input_mappings_keyboard_count) *  sizeof(input_buttonmap);	
		
		input_mappings_keyboard = realloc (input_mappings_keyboard, _size);
		
		// loop the list of keyb keys assigned
		for (i = 0; i < input_mappings_keyboard_count;i++)
			fread(handle,mappedcontrol_); // writes each one of the keys assigned
			
			input_mappings_keyboard[i].control = mappedcontrol_.control;
			input_mappings_keyboard[i].device_id = mappedcontrol_.device_id;
			input_mappings_keyboard[i].player = mappedcontrol_.player;
			input_mappings_keyboard[i].axisnumber = mappedcontrol_.axisnumber;
			input_mappings_keyboard[i].hatnumber = mappedcontrol_.hatnumber;
			input_mappings_keyboard[i].value = mappedcontrol_.value;
		end
	end
	
	// joystick
	fread(handle,input_mappings_joystick_count); // writes the quantity of joy buttons assigned	
	if (input_mappings_joystick_count == 0)
		free(input_mappings_joystick); // libera mem del puntero viejo
		input_mappings_joystick = null;
	else
		// redimencionar tamaño del puntero
		_size = (input_mappings_joystick_count) *  sizeof(input_buttonmap);	
		input_mappings_joystick = realloc (input_mappings_joystick, _size);
		
		// loop the list of joy buttons assigned
		for (i = 0; i < input_mappings_joystick_count;i++)
			fread(handle,mappedcontrol_); // writes each one of the keys assigned
			
			input_mappings_joystick[i].control = mappedcontrol_.control;
			input_mappings_joystick[i].device_id = mappedcontrol_.device_id;
			input_mappings_joystick[i].player = mappedcontrol_.player;
			input_mappings_joystick[i].axisnumber = mappedcontrol_.axisnumber;
			input_mappings_joystick[i].hatnumber = mappedcontrol_.hatnumber;
			input_mappings_joystick[i].value = mappedcontrol_.value;
		end
    end
	
	fclose(handle);                 // zipping up after business is done
End

/**
 * Returns an string describing the devices buttons assigned to an action
 */
function string input_get_assigned(player, button)

private
int i;
input_buttonmap pointer mappedcontrol_;
string description = "";
axis_action = "";
joy = false;
begin
    
    // busca una tecla configurada para el control
    for (i = 0; i < input_mappings_joystick_count;i++) 
        axis_action = '';
        mappedcontrol_ = input_mappings_joystick[i];
        if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
            ((player != 0 and mappedcontrol_.player == player) or (player == 0)) and mappedcontrol_.device_id > -1)
            
            if(mappedcontrol_.axisnumber > -1 and mappedcontrol_.control == button )
                
                if (mappedcontrol_.axisnumber == 1)
                    if (mappedcontrol_.value <0)
                        axis_action = 'UP';
                    elseif (mappedcontrol_.value >0)
                        axis_action = 'DOWN';
                    end
                end
                if (mappedcontrol_.axisnumber == 0)
                    if (mappedcontrol_.value >0)
                        axis_action = 'RIGHT';
                    elseif (mappedcontrol_.value <0)
                        axis_action = 'LEFT';
                    end
                end

                description = description +  "Joy"+ (mappedcontrol_.device_id+1)+": " + axis_action; // axis

            elseif(mappedcontrol_.hatnumber > -1 and mappedcontrol_.control == (button  - 10000))
                //description = description +  "J " + mappedcontrol_.device_id + ": Hat " +  mappedcontrol_.hatnumber + " " + mappedcontrol_.value + ". "; // hat
            else            
                description = description +  "Joy" + (mappedcontrol_.device_id+1) + ": Btn " + mappedcontrol_.value; // Button
            end

            joy = true;
        end
    end

    //solo muestra la config de teclado si no hay joystick
    if (!joy or description == '')
        // busca una tecla configurada para el control
        for (i = 0; i < input_mappings_keyboard_count;i++) 
            mappedcontrol_ = input_mappings_keyboard[i];
            if (mappedcontrol_.control == button and 
                ((player != 0 and mappedcontrol_.player == player) or (player == 0)))           
                
                description = description + "Kb:" + input_scancodes[(mappedcontrol_.value - 1)] + " ";
            end
        end
    end
    
    if (description=='')
        description = 'None';
    end
    return trim(description);
end

/**
 * clones all controls to another action
 */
function input_clone_button(player, button, button_destination)
private
int i;
input_buttonmap pointer mappedcontrol_;

input_buttonmap pointer new_input_mappings_keyboard;
int new_input_mappings_keyboard_count = 0;

input_buttonmap pointer new_input_mappings_joystick;
int new_input_mappings_joystick_count = 0;

int _size = 0;
begin
    if (input_mappings_keyboard != NULL)
        // busca una tecla configurada para el control
        new_input_mappings_keyboard_count = input_mappings_keyboard_count;
        for (i = 0; i < new_input_mappings_keyboard_count;i++) 
            mappedcontrol_ = input_mappings_keyboard[i];
            if (mappedcontrol_.control == button and 
                ((player != 0 and mappedcontrol_.player == player) or (player == 0)))
                
                // calcula el nuevo tamaño
                _size = (input_mappings_keyboard_count+1) *  sizeof(input_buttonmap);
                
                
                // redimencionar tamaño del puntero
                input_mappings_keyboard = realloc (input_mappings_keyboard, _size);
                
                input_mappings_keyboard_count++;
                
                // setear el nuevo elemento 
                input_mappings_keyboard[input_mappings_keyboard_count - 1].control = button_destination;
                input_mappings_keyboard[input_mappings_keyboard_count - 1].player = mappedcontrol_.player;
                input_mappings_keyboard[input_mappings_keyboard_count - 1].value = mappedcontrol_.value;
                
            else
                
                continue; // si no es del player y el boton indicado no lo pasamos al nuevo puntero             
                            
            end
        end
    end
    
    _size = 0;
    
    if (input_mappings_joystick != NULL)
        // busca una tecla configurada para el control
        new_input_mappings_joystick_count = input_mappings_joystick_count;
        for (i = 0; i < new_input_mappings_joystick_count;i++) 
            mappedcontrol_ = input_mappings_joystick[i];
            if ((mappedcontrol_.control == button or mappedcontrol_.control == (button  - 10000)) and 
                ((player != 0 and mappedcontrol_.player == player) or (player == 0)))
                
            
                // calcula el nuevo tamaño
                _size = (input_mappings_joystick_count+1) *  sizeof(input_buttonmap);
                
                
                // redimencionar tamaño del puntero
                input_mappings_joystick = realloc (input_mappings_joystick, _size);
                
                input_mappings_joystick_count++;
                
                // setear el nuevo elemento 
                input_mappings_joystick[input_mappings_joystick_count - 1].control = button_destination;
                input_mappings_joystick[input_mappings_joystick_count - 1].player = mappedcontrol_.player;
                input_mappings_joystick[input_mappings_joystick_count - 1].value = mappedcontrol_.value;
                input_mappings_joystick[input_mappings_joystick_count - 1].device_id = mappedcontrol_.device_id;
                input_mappings_joystick[input_mappings_joystick_count - 1].axisnumber = mappedcontrol_.axisnumber;
                input_mappings_joystick[input_mappings_joystick_count - 1].hatnumber = mappedcontrol_.hatnumber;
            else
                continue;
            end
        end 
    end
    
    
    return;

end

#endif