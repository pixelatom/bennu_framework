include "../library/dictionary.inc";

#ifndef __EVENTS
#define __EVENTS

import "mod_proc";

local
	string __event_name;
	_dictionary __event_args;	
end

/**
 * Events are process that last one frame only
 */
process event(string __event_name)
begin
	//priority = 255;
	frame;
	
onexit:
	dictionary_clear(&__event_args);	
end

function event_get_process(event e)
begin
	return e.father;
end

function event_get_process_type(event e)
begin
	return e.father.reserved.process_type;
end

function string event_get_name(event e) 
begin
	return e.__event_name;
end

function event_set_arg(event e, string name, int value)
begin
	return dictionary_set(&(e.__event_args), name, value);
end

function event_get_arg(event e, string name)
begin
	return dictionary_get(&(e.__event_args), name);
end

#define trigger(event_name) event(event_name)

/**
 * On() clause check for event name and run code until END
 * do not use with get_id because it will break!
 */
#define on(event_name,e) frame(0); while (e=get_id(0)) if (e.__event_name != event_name) continue; end;
#endif