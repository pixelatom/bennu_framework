#ifndef __LISTS_LIB
#define __LISTS_LIB


import "mod_mem"; 
import "mod_say"; 

// first item is alway a node so when searching we should always jump the first one
type _int_list
	_int_list pointer next_node = null;
	int value;
end

// strings lists supports strings up to 256 characters
type _string_list
	_string_list pointer next_node = null;
	char value[255];
end


function int_list_add(_int_list pointer list, int value)
private 
	_int_list pointer new_node=null;
	_int_list pointer last_node = null;
begin

	// alloc mem for new list new_node
	new_node = alloc(sizeof(_int_list));

	// stores the value
	new_node.value = value;
	new_node.next_node = null;
	// sets the pointer into the list 
	last_node = list;
	while (last_node.next_node != null)
		last_node = last_node.next_node;
	end
	last_node.next_node = new_node;
	
	return true;
end

function string_list_add(_string_list pointer list, string value)
private 
	_string_list pointer new_node=null;
	_string_list pointer last_node = null;
begin

	// alloc mem for new list new_node
	new_node = alloc(sizeof(_string_list));

	// stores the value
	new_node.value = value;
	new_node.next_node = null;
	// sets the pointer into the list 
	last_node = list;
	while (last_node.next_node != null)
		last_node = last_node.next_node;
	end
	last_node.next_node = new_node;
	
	return true;
end

function int_list_get(_int_list pointer list, int position)
private 
	_int_list pointer node = null;
	int count = -1;
begin
	// loop the nodes list until position
	node = list;
	repeat 

		// return value
		if (count == position)
			return node.value;
		end

		node = node.next_node;
		count++;
	until(node == null)

	return false;
end



function string string_list_get(_string_list pointer list, int position)
private 
	_string_list pointer node = null;
	int count = -1;
begin
	// loop the nodes list until position
	node = list;
	repeat 

		// return value
		if (count == position)
			return node.value;
		end

		node = node.next_node;
		count++;
	until(node == null)

	return false;
end

function int_list_set(_int_list pointer list, int position, int value)
private 
	_int_list pointer node = null;
	int count = -1;
begin
	// loop the nodes list until position
	node = list;
	repeat 

		// return value
		if (count == position)
			return (node.value = value);
		end

		node = node.next_node;
		count++;
	until(node == null)

	return false;
end

function string_list_set(_string_list pointer list, int position, string value)
private 
	_string_list pointer node = null;
	int count = -1;
begin
	// loop the nodes list until position
	node = list;
	repeat 

		// return value
		if (count == position)
			return (node.value = value);
		end

		node = node.next_node;
		count++;
	until(node == null)

	return false;
end

function int_list_remove(_int_list pointer list, int position)
private 
	_int_list pointer node = null; 
	_int_list pointer last_node = null;
	int count = -1;
begin
	// loop the nodes list until position
	last_node = list;
	node = list;
	repeat 
		if (count == position)
			// updates pointer to next_node item
			last_node.next_node = node.next_node;
			// frees memory allocated for new_node in position
			free(node);
			return true;
		end

		last_node = node;
		node = node.next_node;
		count++;
	until(node == null)
	return false;
end

function string_list_remove(_string_list pointer list, int position)
private 
	_string_list pointer node = null; 
	_string_list pointer last_node = null;
	int count = -1;
begin
	// loop the nodes list until position
	last_node = list;
	node = list;
	repeat 
		if (count == position)
			// updates pointer to next_node item
			last_node.next_node = node.next_node;
			// frees memory allocated for new_node in position
			free(node);
			return true;
		end

		last_node = node;
		node = node.next_node;
		count++;
	until(node == null)
	return false;
end

function int_list_clear(_int_list pointer list)
private 
	_int_list pointer node = null;
	_int_list pointer last_node = null;
begin
	// while pointer to next_node item
	// loop the nodes list until position

	// FIRST ITEM SHOULD BE IGNORED
	last_node = list.next_node;
	node = list.next_node;
	repeat
		if (node != null)
			// store pointer to next_node item
			last_node = node;
			node = last_node.next_node;

			// free last_node item
			free(last_node);
		end
	until (node == null)

	list.next_node = null;

	return true;
end

function string_list_clear(_string_list pointer list)
private 
	_string_list pointer node = null;
	_string_list pointer last_node = null;
begin
	// while pointer to next_node item
	// loop the nodes list until position

	// FIRST ITEM SHOULD BE IGNORED
	last_node = list.next_node;
	node = list.next_node;
	repeat
		if (node != null)
			// store pointer to next_node item
			last_node = node;
			node = last_node.next_node;

			// free last_node item
			free(last_node);
		end
	until (node == null)

	list.next_node = null;

	return true;
end

function int_list_count(_int_list pointer list)
private 
	_int_list pointer node = null;
	int count = -1;
begin
	// loop the nodes list until position
	node = list;
	repeat
		node = node.next_node;
		count++;
	until(node == null)

	return count;
end

function string_list_count(_string_list pointer list)
private 
	_string_list pointer node = null;
	int count = -1;
begin
	// loop the nodes list until position
	node = list;
	repeat
		node = node.next_node;
		count++;
	until(node == null)

	return count;
end

#endif