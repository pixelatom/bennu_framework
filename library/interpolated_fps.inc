#ifndef _frames_in_a_second
#define _frames_in_a_second

import "mod_video";

include "../library/previous_status.inc";
include "../library/smart_freeze.inc";

global 
fill_in_count;
end 
process interpolated_fps(key_frames, max_fps)
private
	i=0;
	j=0;
	add_on_frame_at=0;
	key_frame_at=0; 
	key_frame_duration=0;

	float accumulated_frame_duration = 0;
    
	float accumulated_fill_frames = 0;	
	frames_in_a_second;
begin
	
	// main loop should execute at the very last
	
	set_fps(0,0);// unlock FPS to calculate the right duration of the key frame
	
	// The current process ignores the kill signal from now on
	signal_action(S_KILL,S_IGN);
	signal_action(S_FREEZE,S_IGN);
	signal_action(S_SLEEP,S_IGN);
	
	// executes after all the other process
	priority = -250;

	frame;

	loop
		// after all process ran and before the current key frame is rendered

		// we calculate how much a key frame takes
		key_frame_duration = (get_timer() - key_frame_at); // in milliseconds
		if (key_frame_duration<=0)
			key_frame_duration=1;
		end
		
		// amount of frames we can show in a second			
		frames_in_a_second = round(1000/(key_frame_duration)); // key frames in a second
		if (frames_in_a_second<=0)
			frames_in_a_second = 1;
		end		
		
		if (frames_in_a_second > max_fps and max_fps>0)
			frames_in_a_second = max_fps;
		end 
		
		// adjust key frame duration after applying max fps
		key_frame_duration = (int) (1000/ ((float) frames_in_a_second));

		// frame duration adjustment for decimal values
		accumulated_frame_duration += ((1000/ ((float) frames_in_a_second)) - ((float)key_frame_duration));		
		if (accumulated_frame_duration>=1.0)
			key_frame_duration += (int) accumulated_frame_duration;
			accumulated_frame_duration = accumulated_frame_duration - 1.0;
		end

		// we have already shown the previous key frame, we need to wait a little bit to even its duration to the fill-in frames
		while (key_frame_duration >  (get_timer() - key_frame_at))
			From j=0 To 100; End
		end
		
		if (frames_in_a_second > key_frames ) // if we can add frames in between before showing the key frame
			
			// fill_in_count is the amount of frames we can add between each key frame - 1 (the key frame we are going to show)

			accumulated_fill_frames += ( ((float)frames_in_a_second) / ((float)key_frames) ) - 1.0;// sum accumulated fill in frames for decimal values					
			fill_in_count = (int) (accumulated_fill_frames);
			accumulated_fill_frames = accumulated_fill_frames - ((float)fill_in_count);
			
			if (fill_in_count>0)				
				smart_freeze();// sleep all processes but this one

				save_current_status(); // save the status of the key frame

				// Transiciona todos los procesos entre frame anterior y actual x cantidad de veces - 1 ( el frame principal es el último paso)
				for (i=1;i<=fill_in_count;i++)
					// take time here
					add_on_frame_at = get_timer(); 
					
					transition_status(fill_in_count + 1 ,i);
					
					frame; // render transition frames
					
					//if ((graph_mode & MODE_WAITVSYNC) == 0) // only if vsync is disabled

						// calculate render duration and wait rest of frame duration
						while (key_frame_duration >  (get_timer() - add_on_frame_at))
							//From j=0 To 10; End
						end
					//end
					
				end		

				// vuelve al estado del key frame antes de mostrar
				set_current_status();

				// wake up all processes again
				smart_wakeup();

			end
			
		end
		
		// save key frame state
		save_previous_status(); 


		// save key frame start time
		key_frame_at = get_timer(); 

		if (key_frame_at<=0)
			key_frame_at=1;
		end		

		// show key frame
		frame;
		
	end	
end



#endif