#ifndef __PREVIOUS_STATUS
#define __PREVIOUS_STATUS
/**
 * library for saving processes previous frame status
 */
local
	struct _previous
		bool initialized = false;
		angle = 0;
		cnumber= 0;
		ctype = C_SCREEN;
		file;
		flags=0;
		graph;
		region=0;
		size=100;
		size_x=100;
		size_y=100;
		x=0;
		y=0;
		z=0;
		center_x=0;
		center_y=0;
		alpha;
	end
	struct _current
		bool initialized = false;
		angle = 0;
		cnumber= 0;
		ctype = C_SCREEN;
		file;
		flags=0;
		graph;
		region=0;
		size=100;
		size_x=100;
		size_y=100;
		x=0;
		y=0;
		z=0;
		center_x=0;
		center_y=0;
		alpha=255;
	end
	struct _previous_scroll[9]
		bool initialized = false;	
		INT x0; // The X-coordinate of the foreground graphic.*
		INT y0; // The Y-coordinate of the foreground graphic.*
		INT x1; // The X-coordinate of the the background graphic.*
		INT y1; // The Y-coordinate of the the background graphic.*
		INT z; // The Z-coordinate of the scroll window (default: 512).**
	end
	struct _current_scroll[9]
		bool initialized = false;	
		INT x0; // The X-coordinate of the foreground graphic.*
		INT y0; // The Y-coordinate of the foreground graphic.*
		INT x1; // The X-coordinate of the the background graphic.*
		INT y1; // The Y-coordinate of the the background graphic.*
		INT z; // The Z-coordinate of the scroll window (default: 512).**
	end
end



/**
 * Save current status as previous
 */
function save_previous_status()
private 
	proc;
	i;
begin
	while (proc = get_id(0))
		_process_save_previous_status(proc);
	end
	for(i=0;i<=9;i++)	
		_previous_scroll[i].initialized = true;
		_previous_scroll[i].x0 = scroll[i].x0;
		_previous_scroll[i].x1 = scroll[i].x1;
		_previous_scroll[i].y0 = scroll[i].y0;
		_previous_scroll[i].y1 = scroll[i].y1;
		_previous_scroll[i].z = scroll[i].z;
	end
end

function _process_save_previous_status(proc)
private
	int center_x, center_y;
begin
	proc._previous.initialized = true;
	proc._previous.angle = proc.angle;
	proc._previous.cnumber = proc.cnumber;
	proc._previous.ctype = proc.ctype;
	proc._previous.file = proc.file;
	proc._previous.flags = proc.flags;
	proc._previous.graph = proc.graph;
	proc._previous.region = proc.region;
	proc._previous.size = proc.size;
	proc._previous.size_x = proc.size_x;
	proc._previous.size_y= proc.size_y;
	proc._previous.x = proc.x;
	proc._previous.y = proc.y;
	proc._previous.z = proc.z;
	proc._previous.alpha = proc.alpha;

	if (fpg_exists(proc.file) and map_exists(proc.file , proc.graph))
		get_point (proc.file , proc.graph , 0 , &center_x, &center_y);
		proc._previous.center_x = center_x;
		proc._previous.center_y = center_y;
	else
		proc._previous.center_x = null;
		proc._previous.center_y = null;
	end

end

/**
 * Save current status
 */
function save_current_status()
private 
	proc;
	i;
	int center_x, center_y;
begin
	while (proc = get_id(0))
		proc._current.initialized = true;
		proc._current.angle = proc.angle;
		proc._current.cnumber = proc.cnumber;
		proc._current.ctype = proc.ctype;
		proc._current.file = proc.file;
		proc._current.flags = proc.flags;
		proc._current.graph = proc.graph;
		proc._current.region = proc.region;
		proc._current.size = proc.size;
		proc._current.size_x = proc.size_x;
		proc._current.size_y= proc.size_y;
		proc._current.x = proc.x;
		proc._current.y = proc.y;
		proc._current.z = proc.z;
		proc._current.alpha = proc.alpha;

		if (fpg_exists(proc.file) and map_exists(proc.file , proc.graph))
			get_point (proc.file , proc.graph , 0 , &center_x, &center_y);
			proc._current.center_x = center_x;
			proc._current.center_y = center_y;
		else
			proc._current.center_x = null;
			proc._current.center_y = null;
		end
	end
	for(i=0;i<=9;i++)	
		_current_scroll[i].initialized = true;
		_current_scroll[i].x0 = scroll[i].x0;
		_current_scroll[i].x1 = scroll[i].x1;
		_current_scroll[i].y0 = scroll[i].y0;
		_current_scroll[i].y1 = scroll[i].y1;
		_current_scroll[i].z = scroll[i].z;
	end
end

/**
 * Set processes status = to current
 */
function set_current_status()
private 
	proc;
	i;
begin
	while (proc = get_id(0))
		if (proc._current.initialized)
			proc.angle = proc._current.angle;
			proc.cnumber = proc._current.cnumber;
			proc.ctype = proc._current.ctype;
			proc.file = proc._current.file;
			proc.flags = proc._current.flags;
			proc.graph = proc._current.graph;
			proc.region = proc._current.region;
			proc.size = proc._current.size;
			proc.size_x = proc._current.size_x;
			proc.size_y= proc._current.size_y;
			proc.x = proc._current.x;
			proc.y = proc._current.y;
			proc.z = proc._current.z;
			proc.alpha = proc._current.alpha;

			if (proc._current.center_x != null)
				if (fpg_exists(proc.file) and map_exists(proc.file , proc.graph))
					center_set (proc.file , proc.graph , proc._current.center_x , proc._current.center_y);
				end
			end
		end
	end
	for(i=0;i<=9;i++)	
		if (_current_scroll[i].initialized == true)
			scroll[i].x0 = _current_scroll[i].x0;
			scroll[i].x1 = _current_scroll[i].x1;
			scroll[i].y0 = _current_scroll[i].y0;
			scroll[i].y1 = _current_scroll[i].y1;
			scroll[i].z = _current_scroll[i].z;
		end
	end
end


function set_previous_status()
private 
	proc;
	i;
begin
	while (proc = get_id(0))
		if (proc._previous.initialized)
			proc.angle = proc._previous.angle;
			proc.cnumber = proc._previous.cnumber;
			proc.ctype = proc._previous.ctype;
			proc.file = proc._previous.file;
			proc.flags = proc._previous.flags;
			proc.graph = proc._previous.graph;
			proc.region = proc._previous.region;
			proc.size = proc._previous.size;
			proc.size_x = proc._previous.size_x;
			proc.size_y= proc._previous.size_y;
			proc.x = proc._previous.x;
			proc.y = proc._previous.y;
			proc.z = proc._previous.z;
			proc.alpha = proc._previous.alpha;

			if (proc._previous.center_x != null)
				if (fpg_exists(proc.file) and map_exists(proc.file , proc.graph))
					center_set (proc.file , proc.graph , proc._previous.center_x , proc._previous.center_y);
				end
			end
		end
	end
	for(i=0;i<=9;i++)	
		if (_previous_scroll[i].initialized == true)
			scroll[i].x0 = _previous_scroll[i].x0;
			scroll[i].x1 = _previous_scroll[i].x1;
			scroll[i].y0 = _previous_scroll[i].y0;
			scroll[i].y1 = _previous_scroll[i].y1;
			scroll[i].z = _previous_scroll[i].z;
		end
	end
end

/**
 * Set a intermediate transition_step between previous and current status
 */
function transition_status(total,transition_step)
private 
	proc;
	i;
begin
	while (proc = get_id(0))
		if (proc._previous.initialized == true and proc._current.initialized == true)

			if (region_out(proc,proc.region))
				continue;
			end

			if (proc._previous.angle != proc._current.angle)
				proc.angle = (proc._previous.angle) + ((( (float)(proc._current.angle) - (float)(proc._previous.angle) ) / (float)(total)) * (float)transition_step);
			end			
			if (proc._previous.size != proc._current.size)
				proc.size = (proc._previous.size) + ((( (float)(proc._current.size) - (float)(proc._previous.size) ) / (float)(total)) * (float)transition_step);
			end
			if (proc._previous.size_x != proc._current.size_x)
				proc.size_x = (proc._previous.size_x) + ((( (float)(proc._current.size_x) - (float)(proc._previous.size_x) ) / (float)(total)) * (float)transition_step);
			end
			if (proc._previous.size_y != proc._current.size_y)
				proc.size_y = (proc._previous.size_y) + ((( (float)(proc._current.size_y) - (float)(proc._previous.size_y) ) / (float)(total)) * (float)transition_step);
			end
			if (proc._previous.x != proc._current.x)
				//exit('yes');
				proc.x = (proc._previous.x) + ((( (float)(proc._current.x) - (float)(proc._previous.x) ) / (float)(total)) * (float)transition_step);
			end
			if (proc._previous.y != proc._current.y)				
				proc.y = (proc._previous.y) + ((( (float)(proc._current.y) - (float)(proc._previous.y) ) / (float)(total)) * (float)transition_step);
				//say(proc.y);
			end
			if (proc._previous.z != proc._current.z)
				proc.z = (proc._previous.z) + ((( (float)(proc._current.z) - (float)(proc._previous.z) ) / (float)(total)) * (float)transition_step);
			end

			if (proc._previous.alpha != proc._current.alpha)
				proc.alpha = (proc._previous.alpha) + ((( (float)(proc._current.alpha) - (float)(proc._previous.alpha) ) / (float)(total)) * (float)transition_step);
			end

			if (proc._previous.center_x != null and proc._current.center_x != null 
				and (proc._previous.center_x != proc._current.center_x OR  proc._previous.center_y != proc._current.center_y)
				and proc._previous.file == proc._current.file 
				and proc._previous.graph == proc._current.graph )
				if (fpg_exists(proc.file) and map_exists(proc.file , proc.graph))
					center_set (proc.file , proc.graph , 
						(proc._previous.center_x) + ((( (float)(proc._current.center_x) - (float)(proc._previous.center_x) ) / (float)(total)) * (float)transition_step),
						(proc._previous.center_y) + ((( (float)(proc._current.center_y) - (float)(proc._previous.center_y) ) / (float)(total)) * (float)transition_step));
				end
			end
		end
	end
	for(i=0;i<=9;i++)	
		if (_current_scroll[i].initialized == true and _previous_scroll[i].initialized == true)
			if (_current_scroll[i].x0 != _previous_scroll[i].x0)
				scroll[i].x0 = (_previous_scroll[i].x0) + ((( (float)(_current_scroll[i].x0) - (float)(_previous_scroll[i].x0) ) / (float)(total)) * (float)transition_step);
			end
			if (_current_scroll[i].x1 != _previous_scroll[i].x1)
				scroll[i].x1 = (_previous_scroll[i].x1) + ((( (float)(_current_scroll[i].x1) - (float)(_previous_scroll[i].x1) ) / (float)(total)) * (float)transition_step);
			end
			if (_current_scroll[i].y0 != _previous_scroll[i].y0)
				scroll[i].y0 = (_previous_scroll[i].y0) + ((( (float)(_current_scroll[i].y0) - (float)(_previous_scroll[i].y0) ) / (float)(total)) * (float)transition_step);
			end
			if (_current_scroll[i].y1 != _previous_scroll[i].y1)
			scroll[i].y1 = (_previous_scroll[i].y1) + ((( (float)(_current_scroll[i].y1) - (float)(_previous_scroll[i].y1) ) / (float)(total)) * (float)transition_step);
			end
			if (_current_scroll[i].z != _previous_scroll[i].z)
				scroll[i].z  = (_previous_scroll[i].z) + ((( (float)(_current_scroll[i].z) - (float)(_previous_scroll[i].z)) / (float)(total)) * (float)transition_step);
			end
		end
	end
end

#endif