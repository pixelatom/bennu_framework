#ifndef __SMART_FREEZE
#define __SMART_FREEZE

include "../library/lists.inc";
/**
 * Function to freeze all process except the caller one
 * and the ones that were already frozen, se we can keep them
 * frozen after waking up the ones we froze
 */

import "mod_proc";


global
	_int_list smart_frozen_ids;
end

function smart_freeze()
private 
	proc;
begin
	if (int_list_count(&smart_frozen_ids)>0)
		smart_wakeup();
	end
	while (proc = get_id(0))
		if(proc.reserved.status != STATUS_FROZEN and proc != father)
			signal(proc,s_freeze);
			int_list_add(&smart_frozen_ids,proc);
		end
	end
end

function smart_wakeup()
private 
	i,proc;
begin
	for(i=0;i<int_list_count(&smart_frozen_ids);i++)
		proc = int_list_get(&smart_frozen_ids,i);
		if (exists(proc))
			signal(proc,S_WAKEUP);
		end
	end
	int_list_clear(&smart_frozen_ids);
end


#endif