#ifndef __FILE_ANIM
#define __FILE_ANIM

type _anim_x_y
	x = 0;
	y = 0;
end

local 
	// vars usadas por la animacion	
	byte pointer anim_pointer = null;
	_anim_x_y pointer anim_pos_pointer = null; // Esta es el defasaje de la animacion en X e Y
	byte anim_count = 0;
	byte anim_pos = 0;
	bool anim_loop = true;
	bool anim_started = false;
end


/**
 * set the current animation (if it's not already the current)
 */
function set_anim(process_id,byte pointer anim_pointer,count,anim_loop)
begin
	if(process_id.anim_pointer != anim_pointer)
		process_id.anim_pos = 0;
		process_id.anim_pointer = anim_pointer;
		process_id.anim_count = count;
		process_id.anim_loop = anim_loop;
		process_id.anim_pos_pointer = null;
		process_id.anim_started = false;

		return true;
	end
	return false;
end
/**
 * set the current animation with animation position info
 */
function set_anim_with_pos(process_id,byte pointer anim_pointer,count,anim_loop,anim_pos,_anim_x_y pointer anim_pos_pointer)
begin
	if(process_id.anim_pointer != anim_pointer)
		process_id.anim_pos = anim_pos;
		process_id.anim_pointer = anim_pointer;
		process_id.anim_count = count;
		process_id.anim_loop = anim_loop;
		process_id.anim_pos_pointer = anim_pos_pointer;
		process_id.anim_started = false;

		return true;
	end
	return false;
end

/**
 * return the pointer of current animation 
 */
function anim_get(process_id)
begin	
	return process_id.anim_pointer;
end

/**
 * set the frame of the current animation 
 */
function anim_set_frame(process_id,pos)
begin	
	process_id.anim_pos = pos;
	process_id.anim_started = false;
end

/**
 * get the frame of the current animation 
 */
function anim_get_frame(process_id)
begin	
	return process_id.anim_pos;
end

/**
 * returns true if the current anim has reached its last frame
 */
function anim_end(process_id)
begin
	if (process_id.anim_pointer == null)
		return true;
	end
	return process_id.anim_pos >= process_id.anim_count -1;
end
/**
 * runs the current animation
 */
function animate()
begin
    if (father.anim_pointer != null)
            // Cambia el frame de animación 
			if (father.anim_started == true)			
				father.anim_pos++;
				
				if (father.anim_pos >= father.anim_count and father.anim_loop)
					father.anim_pos = 0;
				elseif(father.anim_pos >= father.anim_count -1)
					father.anim_pos = father.anim_count - 1;
				end	
			else
				father.anim_started = true;
			end
            
            father.graph = father.anim_pointer[father.anim_pos]; // Muestra el grafico correspondiente
            
            if(father.anim_pos_pointer != null)
				if(father.flags & 1 == 1)
						father.x -= father.anim_pos_pointer[father.anim_pos].x;
				else
						father.x += father.anim_pos_pointer[father.anim_pos].x;
				end
				father.y += father.anim_pos_pointer[father.anim_pos].y;
            end
    end
end


#endif