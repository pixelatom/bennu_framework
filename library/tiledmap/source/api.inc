// TILEDMAP API
function tilemap_load_tileset(_tileset tileset,string image)
private
    int graphic_id,img_width,img_height;
    int tile;
begin
    graph = png_load(image);

    tileset.fpg = fpg_new();

    img_height = graphic_info ( 0 , graph , G_HEIGHT );
    img_width = graphic_info ( 0 , graph , G_WIDTH );


    graphic_id = 0;
    for (y=0;y < img_height; y += tileset.tile_size.y)
        if (y+tileset.tile_size.y > img_height) continue; end
        for (x=0;x < img_width; x += tileset.tile_size.x)
            if (x + tileset.tile_size.x >img_width) continue; end

            graphic_id++;

            // create tile image
            tile = map_new(tileset.tile_size.x,tileset.tile_size.y,16);
            // extracts block from origin
            map_block_copy (0, tile ,0,0, graph , x , y , tileset.tile_size.x , tileset.tile_size.y, 0);
            // insert tile into FPG
            fpg_add ( tileset.fpg , graphic_id , 0 , tile );
            // changes center to 0,0
            center_set ( tileset.fpg , graphic_id , 0 ,0);
            // unload tile
            map_unload(0,tile);


        end
    end

    //fpg_save( tileset.fpg , "test.fpg" );

    // in the end we'll unload the processed image
    map_unload(0,graph);
end

function tilemap_load_map(_tiled_map map, string filepath)
private
	string tmx;
	string match;
	int layercount = 0;

	string tmp;
	int array_size;

	string temp_data[MAX_TILEMAP_SIZE]; // temporal array to read the layer tiles

	int i,j,array_index; // indexes to traverse the arrays

	string path="";
begin

	tmx = file(filepath);

    path = dirname(filepath);
    if (path!="")
        path = path + "/";
    end



	// parses tileset info
	regex( '<tileset[^>]*>', tmx );
	match = regex_reg[0];

	// tile width
	regex('tilewidth="([0-9]*)"',match);
	map.tileset.tile_size.x = regex_reg[1];

	// tile height
	regex('tileheight="([0-9]*)"',match);
	map.tileset.tile_size.y = regex_reg[1];

	// extract tileset image file name
	regex('<image[[:space:]]*source="([^"]*)"[^<]*</tileset>',tmx);
	path = path+regex_reg[1];

	// load tileset
    tilemap_load_tileset(map.tileset, path);
    
    map.objectgroup_count=0;
    map.layercount=0;
    

    // read layers
    while (regex('<(layer|objectgroup)[^>]*>',tmx)>-1 and layercount<10)
        //say(regex_reg[0]);

        // match layer size
        	match = regex_reg[0];
        	
        	switch (regex_reg[1])
            	case "layer":
                	regex('width="([0-9]*)"',match);
                	map.layers[layercount].size.x = regex_reg[1];

                	regex('height="([0-9]*)"',match);
                	map.layers[layercount].size.y = regex_reg[1];
		
	            if (regex('opacity="([0-9.]*)"',match)>-1)
		            map.layers[layercount].opacity =  regex_reg[1];
		        else
		            map.layers[layercount].opacity = 1;
	            end
	            if (regex('visible="([0-9]*)"',match)>-1)
		            map.layers[layercount].visible =  regex_reg[1];
		        else
		            map.layers[layercount].visible = true;
	            end
               	tmx = regex_replace ( match , "", tmx); // removes the processed layer tag from the string

                	// match layer data
                	regex('<data encoding="csv">([0-9,[:space:]]*)</data>',tmx);

                	match = regex_reg[1]; //saves CSV data

                	//say(match);           // prints the matched info just for testing stuff

                	tmx = regex_replace ( regex_reg[0] , "", tmx); // removes the extracted data from the string

                	// proccess csv data

                	array_size = map.layers[layercount].size.y * map.layers[layercount].size.x; // calculate array size

                	map.layers[layercount].map_data = alloc( array_size * sizeof(word)); // alloc memory to store map array

                	// split the csv into a STRING array

                	For(i=0;i<array_size;i++)
                    temp_data[i] = "";
                End

                	match = regex_replace ( '[:space:]' , "", match); // removes spaces from the CSV
                split ( ',' , match , &temp_data , array_size ); // convert to array splitted by ','

                // converts the string array to a word array
                For(i=0;i<array_size;i++)
                    map.layers[layercount].map_data[i] = (word) temp_data[i];
                End

                	layercount++;
            	end
            	case "objectgroup":
            	    
            	    if (regex('name="([^"]*)"',match)>-1)
		            map.objectgroups[map.objectgroup_count].name = regex_reg[1];
	            end
	            
	            tmx = regex_replace ( match , "", tmx); // removes the processed layer tag from the string
	            map.objectgroups[map.objectgroup_count].objectcount = 0;
	            while (regex('^[[:space:]]*<object[^>]*>',tmx)>-1 and map.objectgroups[map.objectgroup_count].objectcount<250)
	                match = regex_reg[0];
	                	
	                	regex('x="([0-9]*)"',match);
                    	map.objectgroups[map.objectgroup_count].objects[map.objectgroups[map.objectgroup_count].objectcount].x = regex_reg[1];
	                
	                	regex('y="([0-9]*)"',match);
                    	map.objectgroups[map.objectgroup_count].objects[map.objectgroups[map.objectgroup_count].objectcount].y = regex_reg[1];
                    	
                    	if (regex('name="([^"]*)"',match)>-1)
	                    map.objectgroups[map.objectgroup_count].objects[map.objectgroups[map.objectgroup_count].objectcount].name =  regex_reg[1];
                    end
                    
                    if (regex('type="([^"]*)"',match)>-1)
	                    map.objectgroups[map.objectgroup_count].objects[map.objectgroups[map.objectgroup_count].objectcount].typename =  regex_reg[1];
                    end
                    
                    if (regex('width="([^"]*)"',match)>-1)
	                    map.objectgroups[map.objectgroup_count].objects[map.objectgroups[map.objectgroup_count].objectcount].width =  regex_reg[1];
                    end
                    
                    if (regex('height="([^"]*)"',match)>-1)
	                    map.objectgroups[map.objectgroup_count].objects[map.objectgroups[map.objectgroup_count].objectcount].height =  regex_reg[1];
                    end
	                
	                tmx = regex_replace ( match , "", tmx); // removes the processed layer tag from the string
	                
	                map.objectgroups[map.objectgroup_count].objectcount++;
	            end
	            
	            
            	    map.objectgroup_count++;
            	end
        	end

        	

	end
	
	map.layercount = layercount;
end

// paint with transparent into a graph
function deleteBox(graph, x, y, width, height)
begin
	drawing_color(0);
	drawing_map(0, graph);
	draw_box(x,y,x+width -1, y+height -1);
End


/**
 * render a scroll layer with the properties of the scroll and the tile layer
 */
process tilemap_layer_scroll(scroll_id, _tile_layer layer, _tileset tileset, float width, float height)
public
    int screen_width, screen_height;
    
    _cord last_pos_rendered = -1,-1;

    int i,j;

    float map_x, map_y, last_map_x, last_map_y;

    _cord tile_start_pos, tile_end_pos ;

    string tmp;

	int layer_map;
	
	int buffer;
end
begin
	priority = -2;
	file = 0;
	
	buffer = 4;
	
	layer_map = new_map(width +( buffer * tileset.tile_size.x),height +( buffer * tileset.tile_size.y) ,16);// create a map to draw the background, + 1 tile size

	// create an extended scroll, drawn on the "scroll_blit_graph".
	//graph = new_map(width ,height,16); // create a map to blit the scroll window on
	define_region (10,    0,   0, width , height ) ;
	start_scroll(scroll_id,file,layer_map,0,10,3/*,file,graph*/);
	
	
	// store render information
	layer.map = layer_map;
	layer.width = width;
	layer.height = height;
    layer.buffer = buffer; 

	//graph = layer_map;
	LOOP
    	    z = SCROLL[scroll_id].z;
		alpha = layer.opacity * 255;
		
		
		if (layer.visible != true) alpha = 0; end
//alpha = 255;
	    // layer boundaries control
	    if (SCROLL[scroll_id].x0 >= (layer.size.x*tileset.tile_size.x) - width )
    	    SCROLL[scroll_id].x0 =  (layer.size.x*tileset.tile_size.x) - width ;
	    end
	    if (SCROLL[scroll_id].x0 < 0)
    	    SCROLL[scroll_id].x0 = 0;
	    end

	    if (SCROLL[scroll_id].y0 >= (layer.size.y*tileset.tile_size.y) - height )
    	    SCROLL[scroll_id].y0 =  (layer.size.y*tileset.tile_size.y) - height ;
	    end
	    if (SCROLL[scroll_id].y0 < 0)
    	    SCROLL[scroll_id].y0 = 0;
	    end

	    tile_start_pos.x = round(SCROLL[scroll_id].x0/tileset.tile_size.x);
	    tile_end_pos.x = (tile_start_pos.x + round((width)/tileset.tile_size.x));

	    tile_start_pos.y = round(SCROLL[scroll_id].y0/ tileset.tile_size.y);
	    tile_end_pos.y = (tile_start_pos.y + round((height)/tileset.tile_size.y));

		if (tile_start_pos.x != last_pos_rendered.x or tile_start_pos.y != last_pos_rendered.y)
			
			for (j = tile_start_pos.y - (buffer/2); j<= tile_end_pos.y + (buffer/2); j++  )

				if (j>=layer.size.y) break; end
				if (j<0) continue; end
	
				// determinates Y position where to paint the tile in the scroll map
				map_y = j / ((height+(tileset.tile_size.y*buffer))/tileset.tile_size.y);
				map_y = map_y - ((int) map_y);
				map_y = round(map_y * (height+(tileset.tile_size.y*buffer)));
				
				// tilepos bug fix					
				if (((int)(map_y)) % tileset.tile_size.y != 0)						
					map_y = ( ((int) (((int) map_y) / tileset.tile_size.y ))  * tileset.tile_size.y);
				end

				for (i = tile_start_pos.x - (buffer/2); i<= tile_end_pos.x + (buffer/2); i++  )
					if (i>=layer.size.x) break; end
					if (i<0) continue; end
		
					// determinates X position where to paint the tile in the scroll map
					map_x = i / ((width+(tileset.tile_size.x*buffer))/tileset.tile_size.x);
					map_x = map_x - ((int) map_x);
					map_x = round(map_x * (width+(tileset.tile_size.x*buffer)));
					
					// tilepos bug fix					
					if (((int)(map_x)) % tileset.tile_size.x != 0)						
						map_x = ( ((int) (((int) map_x) / tileset.tile_size.x ))  * tileset.tile_size.x);
					end
					
					// paints only the difference with the previous position
					if (last_pos_rendered.x == -1 // if it's the first map painting
						// or if we're painting tiles we havent painted before
						or i >= (last_pos_rendered.x + round((width)/tileset.tile_size.x) + (buffer/2)) 
						or i <= last_pos_rendered.x - (buffer/2)
						
						or j >= (last_pos_rendered.y + round((height)/tileset.tile_size.y) + (buffer/2)) 
						or j <= last_pos_rendered.y - (buffer/2))
			
						// paint tile
						deleteBox(layer_map,map_x, map_y , tileset.tile_size.x  ,  tileset.tile_size.y ); // deletes previous tile
						if ((layer.map_data[(j * (layer.size.x )) + i ])>0)
						    map_xputnp ( file , layer_map , tileset.fpg , (layer.map_data[(j * (layer.size.x )) + i ])  , map_x , map_y , 0 , 100 ,100 , 0 ); // paint tile
						end
						//draw_rect ( map_x, map_y , map_x + tileset.tile_size.x  ,  map_y + tileset.tile_size.y );
					end


				End
			end

			last_pos_rendered.x = tile_start_pos.x;
			last_pos_rendered.y = tile_start_pos.y;
		    
		end
		
		FRAME;
	END
onexit
    map_unload(0,layer_map);
    //map_unload(0,graph);
end

function tilemap_get_pixel(_tiled_map map,int layer,int x, int y)
private 
float map_x, map_y;
end
begin

	map_x = x / (map.layers[layer].width+(map.tileset.tile_size.x*map.layers[layer].buffer));
	map_x = map_x - ((int) map_x);
	map_x = round(map_x * (map.layers[layer].width+(map.tileset.tile_size.x*map.layers[layer].buffer)));
	
	map_y = y / (map.layers[layer].height+(map.tileset.tile_size.y*map.layers[layer].buffer));
	map_y = map_y - ((int) map_y);
	map_y = round(map_y * (map.layers[layer].height+(map.tileset.tile_size.y*map.layers[layer].buffer)));
	
	
	return map_get_pixel(0,map.layers[layer].map,map_x,map_y);
	
	
end

process tilemap_start(_tiledmap_viewport viewport, _tiled_map map, float width, float height)
private
layer,i;
begin
	priority = -1;
	viewport.map = map;
	
	for (i=0; i<map.layercount; i++)
		layer 	= tilemap_layer_scroll(i, map.layers[i], map.tileset,  width,  height);
		layer.x = graphic_info(0, 0, G_WIDTH)/2;
		layer.y = graphic_info(0, 0, G_HEIGHT)/2;
		SCROLL[i].z -= i;
		
		viewport.layer_scrolls[i] = layer;
	end
	
	loop
		
		for (i=0; i<map.layercount; i++)
			
			// map boundarie control
			
			if (viewport.x >= (map.layers[i].size.x*map.tileset.tile_size.x) - width )
				viewport.x =  (map.layers[i].size.x*map.tileset.tile_size.x) - width ;
			end
			if (viewport.x < 0)
				viewport.x = 0;
			end
			if (viewport.y >= (map.layers[i].size.y*map.tileset.tile_size.y) - height )
				viewport.y =  (map.layers[i].size.y*map.tileset.tile_size.y) - height ;
			end
			if (viewport.y < 0)
				viewport.y = 0;
			end			
			
			SCROLL[i].x0 = viewport.x;
			SCROLL[i].y0 = viewport.y;
			
			if (exists(viewport.layer_scrolls[i]))
				viewport.layer_scrolls[i].angle = viewport.angle;
				viewport.layer_scrolls[i].size = viewport.zoom;
			end
		end
		frame;
		
		
	end
end



function tilemap_unload_tileset(_tileset tileset)
begin
    fpg_unload(tileset.fpg);
    tileset=null;
end

function tilemap_unload_map(_tiled_map map)
private i=0,j=0; _tiled_map new_map; 
begin
    tilemap_unload_tileset(map.tileset);
    for (i=0;i<map.layercount;i++)
        free(map.layers[i].map_data);
        map.layers[i].map_data = null;
    end
    
    map = new_map;
end

function tiledmap_stop(_tiledmap_viewport viewport)
private
byte i;
_tiledmap_viewport new_viewport;
begin
    for (i=0; i< viewport.map.layercount; i++)
		Stop_scroll(i);
		
		if (exists(viewport.layer_scrolls[i]))
		    signal( viewport.layer_scrolls[i] , s_kill_tree ); 
		    viewport.layer_scrolls[i] = 0;
		end
	end
	viewport = new_viewport;
end

