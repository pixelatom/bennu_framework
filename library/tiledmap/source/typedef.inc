// ENGINE TYPES
/**
 * variable type, used for defining sizes, points or coords
 */
type _cord
	int x=0;
	int y=0;
end

/**
 * A single property user in many different data type
 */
type _property
	char name[250];
	char value[250];
end

/**
 * information retourned after loading a tileset (fpg archive)
 */
type _tileset
	int fpg; // fpg resource identifier
	_cord tile_size; // size of an individual tile	
	_property pointer properties; // array of properties
end

/**
 * an specific tiled map definition (can be defined as an stage, room, whatever)
 */
type _tile_layer
	float opacity = 1;
	byte visible = true;
	short pointer map_data; // pointer to the tiles array
	_cord size; // map size in tiles
	_property pointer properties; // array of properties
	
	
	// information of the render
	
	int map;
	float width;
	float height;
	int buffer;
end

type _tiledmap_object
    char name[250];
    char typename[250];
    int x;
    int y;
    int width;
    int height;
end

type _tiledmap_objectgroup
    char name[250];
    _tiledmap_object objects[250];
    byte objectcount=0;
end

/**
 * combination of a tiled map and its graphical configuration. Tipically used to represent a game stage
 */
type _tiled_map
	_tile_layer layers[9]; // the map layers displayed (each layer is rendered in a scroll)
	_tiledmap_objectgroup objectgroups[9];
	byte objectgroup_count=0;
	_tileset tileset; // the tileset used to build the maps		
	_property pointer properties; // array of properties	
	byte layercount = 0;
end

type _tiledmap_viewport
	_tiled_map map;
	int layer_scrolls[9];
	x,y;
	angle;
	opacity;
	zoom = 100;
end
