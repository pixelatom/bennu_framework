#ifndef __INPUT_LIB
include "../library/controls.inc";
#endif

#ifndef __MOUSE_EVENTS_LIB
include "../library/mouse_events.inc";
#endif

include "../library/events.inc";

#ifndef __UI_BUTTONS
#define __UI_BUTTONS

import "mod_proc";
import "mod_grproc";
import "mod_sound";


Declare Process ui_button(file,normal_graph1,focus_graph1,pressed_graph1,x,y,p_number)
	Public // Declare public variables for the process example_process
		// events
		bool click = false; // the button was pressed
		bool focus = false; // the button received focus
		bool blur  = false; // the button lost focus

		// events sounds
		on_focus_snd = null;
		on_click_snd = null;

		// graphics used to render the button
		normal_graph;
		focus_graph;
		pressed_graph;

		bool has_focus = false;
		byte player_number;	
		
		bool auto_focus = false;

		bool enabled = true;
	End
End

process ui_button(file,normal_graph1,focus_graph1,pressed_graph1,x,y,p_number)
private
	byte _button_pressed = false;	
	ui_button item, previous_item;
	byte another_have_focus;
	bool had_focus = false;
begin	

	normal_graph = normal_graph1;
	focus_graph = focus_graph1;
	pressed_graph = pressed_graph1;
	
	z = father.z - 1;
	
	player_number = p_number;
	
	priority = father.priority;
	
	enabled = true;

	loop
		click = false;
		focus = false;
		blur = false;

		if (!enabled)
			// to do: disabled graph	
			graph = normal_graph;

			has_focus = false;
			
			frame;
			continue;
		end

		// si ningun boton en pantalla tiene foco nos asignamos el foco a nosotros mesmos
		if (!has_focus)
			another_have_focus = ui_button_get_focus(player_number);

			if (!another_have_focus)
				ui_button_set_focus(id);
				
				mouse.x = x;
				mouse.y = y;

				auto_focus = true;
			end
		end
		
		// si se aprieta el boton con el mouse
		if (( (mouse_event(MOUSE_DOWN) and collision(type mouse)) or (has_focus and input_on(p_number,INPUT_UI_OK)) ) and !_button_pressed)
			_button_pressed = true;		
			// le choreamos el foco a los otros botones
			ui_button_set_focus(id);
		elseif (_button_pressed and ((mouse_event(MOUSE_UP) and !collision(type mouse)) or !has_focus))
			_button_pressed = false; // si se suelta el mouse afuera del boton se suelta el boton sin disparar evento click
		elseif (mouse_event(MOUSE_DRAG) or mouse_event(MOUSE_DROP))
			_button_pressed = false; // evitamos disparar click si el usuario esta arrastrando la pantalla		
		elseif (_button_pressed and ((mouse_event(MOUSE_UP) and collision(type mouse)) or (has_focus and !input(p_number,INPUT_UI_OK))))
			_button_pressed = false;			
			click = true;
		elseif(has_focus) // navegacion por controles
			if (input_on(p_number,INPUT_UI_UP) or input_on(p_number,INPUT_UI_LEFT))
				
				item = ui_button_get_next(id);

				if (!item)
					item = ui_button_get_first(player_number);
				end

				if (item)
					ui_button_set_focus(item);
				end
				
			elseif (input_on(p_number,INPUT_UI_DOWN) or input_on(p_number,INPUT_UI_RIGHT))
				item = ui_button_get_previous(id);

				if (!item)
					item = ui_button_get_last(player_number);
				end

				if (item)
					ui_button_set_focus(item);
				end
			end
		end
		
		// asigna grafico 
		if (!_button_pressed)
			if (has_focus)
				graph = focus_graph;
			else
				graph = normal_graph;
			end
		else
			graph = pressed_graph;
		end


		// focus events
		
		if (has_focus != had_focus)
			focus = has_focus == true;
			blur = has_focus == false;
		end

		// event trigger
		if (focus)
			trigger("ui.button.focus");
		end
		if (blur)
			trigger("ui.button.blur");
		end
		if (click)
			trigger("ui.button.click");
		end

		// event reaction

		if (focus and on_focus_snd != null and !auto_focus)
			play_wav(on_focus_snd,0);
		end

		if (click and on_click_snd != null )
			play_wav(on_click_snd,0);
		end
		

		had_focus = has_focus;
		auto_focus = false; // flag telling that the button got the focus itself

		frame;
	end
end

function ui_button_set_focus(ui_button button_id)
private
ui_button item;

begin
	// we can't really avoid executing if the button already has the focus so we execute anyways

	if (!button_id.enabled) 
		return; 
	end
	// removes focus from another buttons
	while (item = get_id(type ui_button))
		if (item.player_number == button_id.player_number)
			item.has_focus = false;
			item.priority = 0;
		end	
	end			
	// applies focus
	button_id.has_focus = true;	
	button_id.priority = 1;
end

// returns the button created before the argument
function ui_button_get_previous(ui_button button_id)
private
ui_button item;
last = false;
begin
	while (item = get_id(type ui_button))
		if (item.player_number == button_id.player_number)
			if (item == button_id)
				return last;
			else
				if (!item.enabled) continue; end
				last = item;
			end
		end
	end	

	return false;
end

function ui_button_get_next(ui_button button_id)
private
ui_button item;
bool return_next = false;
begin
	while (item = get_id(type ui_button))
		
		if (item.player_number == button_id.player_number)
			if (return_next)
				if (!item.enabled) continue; end
				return item;
			end
			if (item == button_id)
				return_next = true;
			end	
		end
	end	

	return false;
end

function ui_button_get_first(byte p_number)
private
ui_button item;
begin
	while (item = get_id(type ui_button))
		if (item.player_number == p_number)
			if (!item.enabled) continue; end
			return item;
		end	
	end	
	return false;
end

function ui_button_get_last(byte p_number)
private
ui_button item;
last = false;
begin
	while (item = get_id(type ui_button))
		if (item.player_number == p_number)
			if (!item.enabled) continue; end
			last = item;
		end	
	end	

	return last;
end

function ui_button_get_focus(byte p_number)
private
ui_button item;
begin
	while (item = get_id(type ui_button))
		//if (item.reserved.status == STATUS_SLEEPING) continue; end
		if (!item.enabled) continue; end
		if (item.player_number == p_number and item.has_focus)
			return item;
		end	
	end	

	return false;
end

#endif