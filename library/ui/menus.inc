include "../library/ui/buttons.inc";
include "../library/motion_tween.inc";
include "../library/screen_info.inc";
include "../library/lists.inc";

#ifndef __UI_MENUS
#define __UI_MENUS

import "mod_map";

// crea y devuelve menu al cual agregar items
// menu_id = menu_create(normal_fnt, onfocus_fnt)

const
	menu_align_left = 0;
	menu_align_right = 1;
	menu_align_center = 2;
end


process ui_menu(_menu_align, _player_number)
public
	// fonts used to render the menu
	normal_fnt, focus_fnt, title_fnt = 0;

	// sound effects assigned to events
	on_focus_snd = null;
	on_click_snd = null;
	on_show_snd	 = null;
	on_hide_snd  = null;

	// text alignment and position relative to the whole screen
	menu_align; // menu_align_left = 0; menu_align_right = 1; menu_align_center = 2;
	offset_x;
	offset_y;
	
	// which player can control the menu	
	player_number;

	// spacing between menu items
	spacing; 

	// list of items
	_int_list items;

	menu_shown = false;

	title="";
begin
	
	
	menu_align = _menu_align; // left, center, right
	player_number = _player_number;
	z = father.z-1;

	loop
		frame;
	end

	onexit:
		int_list_clear(&items);
		signal(id,s_kill_tree);
end

#define ui_menu_new(_menu_align, _player_number) ui_menu(_menu_align, _player_number)



// devuelve id de un proceso al cual consultar por click, aca adentro creamos el mapa que va a usar
// item_id = ui_menu_add_item(text)

process ui_menu_item(ui_menu menu_id,string text)
public
	text_normal_map;
	text_focus_map;
	ui_button btn;
	align;
	string caption;
	
	bool click = false;
	bool focus = false;
	bool blur  = false;
	ui_menu menu;

	_string_list option_captions;
	_int_list option_values;
	int selected_index = 0; // current selected option item
	int value = null; // current selected item value
private
	_previous_selected_index = -1;
begin
	father = menu_id;
	menu = menu_id;

	caption = text;

	//create btn
	btn = ui_button(0, text_normal_map, text_focus_map, text_focus_map, 0, 0, menu_id.player_number);

	btn.z = menu.z;
	 
	ui_menu_item_render(id);

	int_list_add(&menu_id.items, id);

	//signal(btn, s_sleep);
	if (!menu_id.menu_shown)
		btn.enabled = false;
	end

	priority = btn.priority - 1;

	btn.on_focus_snd = menu_id.on_focus_snd;
	btn.on_click_snd = menu_id.on_click_snd;

	while(exists(btn))

		click = btn.click;
		focus = btn.focus;
		blur = btn.blur;

		if (focus)
			trigger("ui.menu.item.focus");
		end
		if (blur)
			trigger("ui.menu.item.blur");
		end
		if (click)
			trigger("ui.menu.item.click");
		end

		// if there are options, we will update selected index
		if (int_list_count(&option_values) > 0 and click)
			selected_index++;
			if (selected_index >= int_list_count(&option_values))
				selected_index = 0;
			end
		end

		// in item, should check for if selected item changed and refresh caption
		if (int_list_count(&option_values) > 0 and _previous_selected_index != selected_index)
			
			// updates value
			value = int_list_get(&option_values,selected_index);

			// updates button images
			ui_menu_item_render(id);

			trigger("ui.menu.item.change");

			_previous_selected_index = selected_index;
		end
		
		frame;
	end

	onexit:
	signal(id,s_kill_tree);
	map_unload(0,text_normal_map);
	map_unload(0,text_focus_map);
	string_list_clear(option_captions);
	int_list_clear(option_values);

end

/**
 * return menu item by index
 */
function ui_menu_get_item(ui_menu menu_id, int index)
begin
	return int_list_get(&menu_id.items, index);
end

/**
 * return item order index in menu
 */
function ui_menu_item_get_index(ui_menu_item menu_item)
private
	i = 0;
begin
	// calculates menu size in pixels
	For(i=0; i<int_list_count(&menu_item.menu.items) ; i++)
		if (menu_item == int_list_get(&menu_item.menu.items,i))
			return i;
		end
	end
	return false;
end

// generates graphics required for the menu item
function ui_menu_item_render(ui_menu_item menu_item_id)
private
	align;
	string caption;
begin
	if (map_exists(0,menu_item_id.text_normal_map))
		map_unload(0,menu_item_id.text_normal_map);
	end
	if (map_exists(0,menu_item_id.text_focus_map))
		map_unload(0,menu_item_id.text_focus_map);
	end

	switch(menu_item_id.menu.menu_align)
		case menu_align_left:
			align = 3;
		end
		case menu_align_center:
			align = 4;
		end
		case menu_align_right:
			align = 5;
		end
	end

	caption = menu_item_id.caption;

	if (int_list_count(&menu_item_id.option_values) > 0)
		caption = caption + ": " + string_list_get(&menu_item_id.option_captions,menu_item_id.selected_index);
	end

	menu_item_id.text_normal_map = write_in_map (menu_item_id.menu.normal_fnt, caption, align);
	menu_item_id.text_focus_map = write_in_map (menu_item_id.menu.focus_fnt, caption, align);			
	menu_item_id.btn.normal_graph = menu_item_id.text_normal_map;
	menu_item_id.btn.focus_graph = menu_item_id.text_focus_map;		
end

#define ui_menu_add_item(menu_id,text) ui_menu_item(menu_id,text)


function ui_menu_item_option(ui_menu_item menu_item, int value, string caption)
begin
	// add option to the item
	int_list_add(&menu_item.option_values, value);
	string_list_add(&menu_item.option_captions, caption);

	// if item selected item is not set, set the first value
	if (menu_item.selected_index == null)
		menu_item.selected_index = 0;
		menu_item.value = value;
	end
end

#define ui_menu_add_item_option(menu_item, value, caption) ui_menu_item_option( menu_item, value, caption)

// set the currently selected option
function ui_menu_set_item_option(ui_menu_item menu_item, int value)
private
	i;
	ui_menu_item_option option;
begin
	// search item with value and set as selected
	// calculates menu size in pixels
	For(i=0; i<int_list_count(&menu_item.option_values) ; i++)
		if (value == int_list_get(&menu_item.option_values,i))
			menu_item.selected_index = i;
			menu_item.value = value;
			return true;
		end
	end

	return false;
end

// elimina el boton y el mapa creado para el item del menu
// menu_remove_item(item_id)

const
	menu_transition_top = 0;
	menu_transition_bottom = 1;
	menu_transition_left = 3;
	menu_transition_right = 4;
end
// hace el menu visible, aca va todo el codigo que ubica los botones, eventualmente va a incluir una animacion pero eso lo dejamos para despues
process ui_menu_show(ui_menu menu_id, menu_transition_from, motion_effect, anim_duration, anim_duration_incremental)
private
	menu_height, menu_width = 0;
	ui_menu_item menu_item;
	_screen_width, _screen_height;
	incremental = 0;
	i = 0;
	tween_to anim;
begin
	// enable items
	For(i=0; i<int_list_count(&menu_id.items) ; i++)
		menu_item = int_list_get(&menu_id.items,i);
		menu_item.btn.enabled = true;
	end

	// focus first item on menu
	ui_menu_set_focus(menu_id);

	// calculates menu size in pixels
	For(i=0; i<int_list_count(&menu_id.items) ; i++)
		menu_item = int_list_get(&menu_id.items,i);
		
		// add menu spacing
		if (menu_height > 0)
			menu_height += menu_id.spacing;
		end

		// add the size of the menu item
		if ( graphic_info(menu_item.file , menu_item.text_normal_map , G_WIDTH) > menu_width)
			menu_width = graphic_info(menu_item.file , menu_item.text_normal_map , G_WIDTH);
		end
		menu_height += graphic_info(menu_item.file , menu_item.text_normal_map , G_HEIGHT);

		// wakes up the menu button that was hidden
		
		//signal(menu_item.btn, s_wakeup);
		//menu_item.btn.enabled = true;
	end

	// get the size of the screen
	_screen_width = get_screen_width();
	_screen_height = get_screen_height();

	// calculates menu position
	switch(menu_id.menu_align)
		case menu_align_left:
			x = 0;
			y = (_screen_height / 2) - (menu_height / 2);
		end
		case menu_align_center:
			x = (_screen_width / 2);
			y = _screen_height / 2 - (menu_height / 2);
		end
		case menu_align_right:
			x = _screen_width;
			y = _screen_height / 2 - (menu_height / 2);
		end
	end

	x += menu_id.offset_x;
	y += menu_id.offset_y;

	// updates pos of the menu proccess so it can uses it as reference
	menu_id.x = x;
	menu_id.y = y;

	if (menu_id.on_show_snd != null )
		play_wav(menu_id.on_show_snd,0);
	end

	For(i=0; i<int_list_count(&menu_id.items) ; i++)
		menu_item = int_list_get(&menu_id.items,i);

		menu_item.btn.x = x;
		menu_item.btn.y = y;

		switch(menu_transition_from)
			case menu_transition_left:
				menu_item.btn.x = -menu_width;
				tween_to(&menu_item.btn.x, x,  motion_effect, anim_duration + incremental);
			end
			case menu_transition_right:
				menu_item.btn.x = _screen_width + menu_width;
				tween_to(&menu_item.btn.x, x,  motion_effect, anim_duration + incremental);
			end
			case menu_transition_top:
				menu_item.btn.y = -graphic_info(menu_item.file , menu_item.text_normal_map , G_HEIGHT);
				tween_to(&menu_item.btn.y, y,  motion_effect, anim_duration + incremental);
			end
			case menu_transition_bottom:
				menu_item.btn.y =  _screen_height + graphic_info(menu_item.file , menu_item.text_normal_map , G_HEIGHT);
				tween_to(&menu_item.btn.y, y,  motion_effect, anim_duration + incremental);
			end
		end

		y += graphic_info(menu_item.file , menu_item.text_normal_map , G_HEIGHT);
		y += menu_id.spacing;

		incremental += anim_duration_incremental; // delays a little bit the animation of the next item
	
	end

	// wait for the anim to finish
	while (_menu_is_animating())

		// skip animation on player input
		if (input_on(menu_id.player_number,INPUT_UI_UP) or input_on(menu_id.player_number,INPUT_UI_LEFT) or 
			input_on(menu_id.player_number,INPUT_UI_DOWN) or input_on(menu_id.player_number,INPUT_UI_RIGHT) or input_on(menu_id.player_number,INPUT_UI_OK))

			while (anim = get_id(type tween_to))
				if (anim.father == id)
					tween_finish(anim.tweenning);
				end
			end
		end

		frame;
	end
	//onexit:
	

	menu_id.menu_shown = true;

	
end

function _menu_is_animating()
private 
anim;
begin
	while (anim = get_id(type tween_to))
		if (anim.father == id.father)
			return true;
		end
	end
	return false;
end

function ui_menu_set_focus(ui_menu menu_id)
private
ui_menu_item menu_item;
begin
	// set focus to the first item of the menu 
	if (int_list_count(&menu_id.items) > 0)
		menu_item = int_list_get(&menu_id.items,0);
		ui_button_set_focus(menu_item.btn);
		menu_item.btn.auto_focus = true;
	end
end


// esconde el menu
process ui_menu_hide(ui_menu menu_id, menu_transition_to, motion_effect, anim_duration, anim_duration_incremental)
private
	menu_height, menu_width = 0;
	ui_menu_item menu_item;
	_screen_width, _screen_height;
	incremental = 0;
	i = 0;
	tween_to anim;
begin

	// calculates menu size in pixels
	For(i=0; i<int_list_count(&menu_id.items) ; i++)
		menu_item = int_list_get(&menu_id.items,i);
		
		// add menu spacing
		if (menu_height > 0)
			menu_height += menu_id.spacing;
		end

		// add the size of the menu item
		if ( graphic_info(menu_item.file , menu_item.text_normal_map , G_WIDTH) > menu_width)
			menu_width = graphic_info(menu_item.file , menu_item.text_normal_map , G_WIDTH);
		end
		menu_height += graphic_info(menu_item.file , menu_item.text_normal_map , G_HEIGHT);
	end


	// get the size of the screen
	_screen_width = get_screen_width();
	_screen_height = get_screen_height();

	if (menu_id.on_show_snd != null )
		play_wav(menu_id.on_hide_snd,0);
	end

	// animates
	For(i = int_list_count(&menu_id.items)-1; i>=0; i--)
		menu_item = int_list_get(&menu_id.items,i);

		menu_item.btn.enabled = false;
		
		switch(menu_transition_to)
			case menu_transition_left:				
				tween_to(&menu_item.btn.x, 0 - menu_width,  motion_effect, anim_duration + incremental);
			end
			case menu_transition_right:				
				tween_to(&menu_item.btn.x, _screen_width + menu_width,  motion_effect, anim_duration + incremental);
			end
			case menu_transition_top:				
				tween_to(&menu_item.btn.y, 0 - menu_height,  motion_effect, anim_duration + incremental);
			end
			case menu_transition_bottom:				
				tween_to(&menu_item.btn.y, _screen_height + menu_height,  motion_effect, anim_duration + incremental);
			end
		end

		incremental += anim_duration_incremental; // delays a little bit the animation of the next item
	
	end

	// wait until animation is done
	while (_menu_is_animating())

		// skip animation on player input
		if (input_on(menu_id.player_number,INPUT_UI_UP) or input_on(menu_id.player_number,INPUT_UI_LEFT) or 
			input_on(menu_id.player_number,INPUT_UI_DOWN) or input_on(menu_id.player_number,INPUT_UI_RIGHT) or input_on(menu_id.player_number,INPUT_UI_OK))

			while (anim = get_id(type tween_to))
				if (anim.father == id)
					tween_finish(anim.tweenning);
				end
			end
		end

		frame;
	end
	
	// sleep menu items
	For(i=0; i<int_list_count(&menu_id.items) ; i++)
		menu_item = int_list_get(&menu_id.items,i);		
		//signal(menu_item.btn, s_sleep);	
		menu_item.btn.enabled = false;	
	end

	menu_id.menu_shown = false;

end

// mata todos los procesos, descarga mapas creados
process ui_menu_destroy(ui_menu menu_id)
private
	i;
	ui_menu_item menu_item;
begin
	For(i=0; i<int_list_count(&menu_id.items) ; i++)
		menu_item = int_list_get(&menu_id.items,i);		
		signal(menu_item, s_kill_tree);		
	end
	signal(menu_id,s_kill_tree);
end

// tenemos unos botones, sobretodo en la seccion de opciones, que usamos como si fuesen select boxes, donde vas cambiando el texto del boton segun el valor que tiene, 
// select_id = menu_add_select(menu_id)

// agregamos opciones para el select
// menu_add_option(select_id,text)

#endif