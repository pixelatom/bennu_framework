import "mod_say";
import "mod_video";
import "mod_text";
import "mod_file";

// includes controls library
include "../library/controls.inc";
private
	string assigned;
begin
	if (file_exists("controls.sav"))
		input_load_config("controls.sav");
	else
		// hay q definir controles de navegacion basicos
		input_map_keyboard(INPUT_UI_CANCEL,0, _esc);
		input_map_keyboard(INPUT_UI_OK,0, _enter);
		
		// configuramos dos teclas para la misma accion
		input_map_keyboard(INPUT_UI_UP,1, _w);	
		input_map_keyboard(INPUT_UI_UP,1, _up);	
		
		// y mapeamos el joystick tambien
		if (joy_number())
			input_map_joystick(INPUT_UI_UP, 1, 0, JOY_HAT_UP);
		end
	end

	set_fps(24,0);
	set_mode(320,240,16);
	
	// lee los controles asignados

	assigned = input_get_assigned(1, INPUT_UI_UP);
	
	say ("Current: " + assigned);
	say ("Press ENTER to assign a button");
	say ("Press ESC to exit");

	// programatically emulates input press
	input_press(1,INPUT_UI_UP);
	frame;

	loop
		
		// checkeamos si se presiona un boton
		if(input(1,INPUT_UI_UP))
			say('Button pressed');
		end
		
		if(input(0,INPUT_UI_CANCEL))
			break;
		end
		
		if(input(0,INPUT_UI_OK))
			input_clear_button(1, INPUT_UI_UP);
			say ("Press ESC to clear buttons");
			say ("Press any button to assign..." );
			if (!input_listen_assign(1, INPUT_UI_UP))
				say ("Buttons cleared");
			end
			assigned = input_get_assigned(1, INPUT_UI_UP);
			input_clone_button(1, INPUT_UI_UP, INPUT_UI_DOWN);
			say ("Current: " + assigned);
			say ("Press ENTER to assign a button");
			say ("Press ESC to exit");
			frame;
		end
		
		
		
		frame;
	end
	
	input_save_config("controls.sav");
	say(":D");
end
