import "mod_say";
import "mod_video";
import "mod_text";
import "mod_file";
import "mod_wm";
import "mod_say";
import "mod_map";

// includes controls library
include "../library/ui/menus.inc";


global
fnt_normal, fnt_focus;
focus_snd = null;
click_snd = null;
show_snd  = null;
hide_snd  = null;
end

process main()
private
	ui_menu_item current_item;
	e;
begin
	// screen config
	set_fps(24,0);
	set_mode(320,240,16);

	// config controls for UI menus
	input_map_keyboard(INPUT_UI_CANCEL,0, _esc);
	input_map_keyboard(INPUT_UI_OK,0, _enter);
	input_map_keyboard(INPUT_UI_UP,1, _up);	
	input_map_keyboard(INPUT_UI_DOWN,1, _down);	

	// load some fonts to be used with the menus
	fnt_normal = fnt_load('../assets/fnt/system_white_small.fnt');
	fnt_focus = fnt_load('../assets/fnt/system_colored_small.fnt');
	focus_snd = load_wav('../assets/snd/nav_move.wav');
	click_snd = load_wav('../assets/snd/nav_select.wav');

	// display main menu
	main_menu();	

	loop
		on("ui.menu.item.focus",e)
			current_item = event_get_process(e);
			tween_to(&current_item.btn.x, current_item.menu.x + 10, motion_effect.regularEaseOut, 3);
		end
		
		on("ui.menu.item.blur",e)
			current_item = event_get_process(e);
			tween_to(&current_item.btn.x, current_item.menu.x , motion_effect.regularEaseOut, 3);
		end

		frame;
	end

end

process main_menu()
private
	ui_menu menu;	
	ui_menu_item item, item1,item2,item3;
	anim;
	e;
begin

	// creates a new menu centered in the screen
	menu = ui_menu_new(menu_align_center,0);

	menu.spacing = 5; // set the spacing between items
	

	// set menu font
	menu.normal_fnt = fnt_normal;
	menu.focus_fnt  = fnt_focus;

	// define some menu event sounds before adding items
	menu.on_focus_snd = focus_snd;
	menu.on_click_snd = click_snd;
	menu.on_show_snd  = null;
	menu.on_hide_snd  = null;

	// add some items
	item1 = ui_menu_add_item(menu, "2 Menues 1 Player");
	item2 = ui_menu_add_item(menu, "Option");

	ui_menu_add_item_option(item2, 0, "No Value");
	ui_menu_add_item_option(item2, 1, "Value 1");
	ui_menu_add_item_option(item2, 2, "Value 2");


	item3 = ui_menu_add_item(menu, "Quit");

	// show the menu with a transition animation
	ui_menu_show(menu, menu_transition_top, motion_effect.regularEaseOut, 5, 5);
	ui_menu_set_focus(menu);

	loop
		// check for menu item events
		on("ui.menu.item.click",e)
			if (item1 == event_get_process(e))
				anim = ui_menu_hide(menu, menu_transition_bottom, motion_effect.regularEaseOut, 5, 5);
				while(exists(anim))
					frame;
				end
				same_player_menu();
				return;
			end

			if (item3 == event_get_process(e))
				anim = ui_menu_hide(menu, menu_transition_bottom, motion_effect.regularEaseOut, 5, 5);
				while (exists(anim))
					frame;
				end
				exit();
			end
		end

		on("ui.menu.item.change",e)
			item = event_get_process(e);
			say ("value: "+item.value);
		end
			

		frame;
	end
	onexit:
		ui_menu_destroy(menu);
end

process same_player_menu()
private
	ui_menu left_menu, right_menu;	
	ui_menu_item item1,item2,item3;
	anim;
begin
	// menu de izquierda
	left_menu = ui_menu_new(menu_align_left,0);
	left_menu.spacing = 5;
	left_menu.offset_x = (get_screen_width()/2) + 10;
	left_menu.normal_fnt = fnt_normal;
	left_menu.focus_fnt  = fnt_focus;
	left_menu.on_focus_snd = focus_snd;
	left_menu.on_click_snd = click_snd;
	left_menu.on_show_snd  = null;
	left_menu.on_hide_snd  = null;

	// menu de derecha
	right_menu = ui_menu_new(menu_align_right,0);
	right_menu.spacing = 5;
	right_menu.offset_x = -(get_screen_width()/2) - 10;
	right_menu.normal_fnt = fnt_normal;
	right_menu.focus_fnt  = fnt_focus;
	right_menu.on_focus_snd = focus_snd;
	right_menu.on_click_snd = click_snd;
	right_menu.on_show_snd  = null;
	right_menu.on_hide_snd  = null;

	// create menu items
	item1 = ui_menu_add_item(right_menu, "Item 1");
	item2 = ui_menu_add_item(right_menu, "Item 2");
	item3 = ui_menu_add_item(right_menu, "Item 3");
	item1 = ui_menu_add_item(left_menu, "Item 4");
	item2 = ui_menu_add_item(left_menu, "Item 5");
	item3 = ui_menu_add_item(left_menu, "Go Back");

	// display both menues
	ui_menu_show(left_menu, menu_transition_right, motion_effect.regularEaseOut, 10, 5);
	ui_menu_show(right_menu, menu_transition_left, motion_effect.regularEaseOut, 10, 5);

	// set focus to the menu on the left
	ui_menu_set_focus(right_menu);
	loop
		if (item3.click)
			anim = ui_menu_hide(right_menu, menu_transition_left, motion_effect.regularEaseOut, 10, 3);
			anim = ui_menu_hide(left_menu, menu_transition_right, motion_effect.regularEaseOut, 10, 3);
			while(exists(anim))
				frame;
			end
			main_menu();
			return;
		end
		frame;
	end
	onexit:
		ui_menu_destroy(right_menu);
end