import "mod_time"
import "mod_map";
import "mod_grproc";
import "mod_key";
import "mod_say";

import "mod_wm";
import "mod_video";
import "mod_scroll";
import "mod_mouse";
import "mod_proc";
import "mod_draw";
import "mod_rand";
import "mod_math";
import "mod_timers";
import "mod_text";
import "mod_screen";

include "../library/controls.inc";
include "../library/hardness_map_physics.inc";

global	
	int foreground_map;
	player_fpg;

	_hmap hardness_map; // the hmap in which the player will move
End;
const
	
	CONTROL_JUMP = 17;
end

Process Main()
private 
i=0;
Begin
	
	
	// shows FPS
	set_fps(0,0);
	write_int( 0 , 10 , 20 , 0 , 0, &fps);
	
	// screen config
	//scale_mode = SCALE_SCALE2X;
	set_mode(640,360,16);
	
	// load resources
	foreground_map = load_png("tiled.png");



	// hardness map config
	hmap_gravity = 1;
	hmap_max_vertical_speed = 12;
	
	hmap_array_mode = true;
	
	// creates hardness map
	hmap_init(&hardness_map, 0, foreground_map,0.1);
	
	// start stage
	center_set(0,foreground_map,0,0);
	start_scroll(0,0,foreground_map,0,0,1);
    x = 0;
    y = 0;
	
	// graph for the ball processes
	graph = new_map(20,20,16);	
	map_clear(0,graph,rgb(255,255,255));
	set_center(0,graph,8,10);
	
	// creates some balls
	From i=50 To 600 step 25;
	    
	    ball(graph,i);
	End
	
	Repeat
        frame;
    Until(key(_ESC))
    // Kill all other process
    let_me_alone();
	exit();
	onexit:
		unload_map(0,graph);
End

Process ball(graph,x)


Begin
	y = 30;
	// tama�o del personaje para el chequeo de durezas
	hmap_box_height = 20;
	hmap_box_width = 20;
	
	hmap_bounce_rate = 1.1;
	loop
		
		// applies hardness map physics to this process
		hmap_apply(&hardness_map);
		
		frame;
	end
	
End

