import "mod_time"
import "mod_map";
import "mod_grproc";
import "mod_key";

import "mod_wm";
import "mod_video";
import "mod_scroll";
import "mod_mouse";
import "mod_proc";
import "mod_draw";
import "mod_rand";
import "mod_math";
import "mod_timers";
import "mod_text";
import "mod_screen";

include "../library/controls.inc";
include "../library/hardness_map_physics.inc";	

global	
	int foreground_map;
	player_fpg;

	_hmap hardness_map; // the hmap in which the player will move
End;
const
	
	CONTROL_JUMP = 17;
end

Process Main()
private
	int background_map;
Begin
	
	
	// shows FPS
	set_fps(24,0);
	write_int( 0 , 10 , 20 , 0 , 0, &fps);

	// escalado de pantalla
	scale_mode = SCALE_SCALE2X;
	set_mode(640,360,16,MODE_HARDWARE);

	
	// load resources
	foreground_map = load_png("tiled.png");

	hmap_array_mode = false;

	// creates hardness map
	hmap_init(&hardness_map, 0, foreground_map,0.1);

	// hardness map config
	hmap_gravity = 6;
	hmap_max_vertical_speed = 20;
	
	
	// start stage
	center_set(0,foreground_map,0,0);
	start_scroll(0,0,foreground_map,0,0,1);
    x = 0;
    y = 0;
	
	// configures player controls
	input_map_keyboard(INPUT_UI_LEFT,1, _a);
	input_map_keyboard(INPUT_UI_RIGHT,1, _d);
	input_map_keyboard(INPUT_UI_DOWN,1, _s);
	
	input_map_keyboard(CONTROL_JUMP,1, _k);
	input_map_keyboard(INPUT_UI_UP,1, _w);
	

	// call player process
	player_process();
	
	
	Repeat
        frame;
    Until(key(_ESC))
    // Kill all other process
    let_me_alone();
	exit();
End

Process player_process()
public 
	string status = "standing";
	
private
	// flags de controles
	
	bool pressed_jump = 0; //puede saltar
	
	i = 0;
	j = 0;
	
	int initial_flag = 0;
	bool on_air = false;
	
	// config de comportamiento
	jump_height = -34;
	char_speed = 0;
	char_max_speed = 18;
	player_number = 1;
	
Begin
	// crea grafico de colision 
	file = 0;		
	
	// tama�o del personaje para el chequeo de durezas
	hmap_box_height = 24;
	hmap_box_width = 16;
	
	graph = new_map(hmap_box_width,hmap_box_height,16);	
	map_clear(0,graph,rgb(255,255,255));
	set_center(0,graph,8,10);
	
	
	x = 200;
	y = 200;
	
	
	
	
	loop
		
		// applies hardness map physics to this process
		hmap_apply(&hardness_map);
		
		
		if(!input(player_number,CONTROL_JUMP) and (status == "standing" or status == "running") and hmap_inc_y == 0)
			pressed_jump = 0;
			hmap_inc_y++; // para que hace esto? IDKLOL
		elseif(input(player_number,CONTROL_JUMP) and hmap_inc_y > 5)
			pressed_jump = 1;		
		elseif(!input(player_number,CONTROL_JUMP) and (status == "standing" or status == "running" ) and hmap_inc_y > 3)	
			// si se esta en el aire cambiamos el estado a jumping
			status = "jumping";	
			on_air = true;
		end
		
		if(!input(player_number,CONTROL_JUMP) and status == "jumping")
			pressed_jump = false;	// para permitir rebote en pared
		end
		

		if(!input(player_number,CONTROL_JUMP) and status == "jumping")
			hmap_inc_y++;
		end
			
		// Motor de estados
		switch(status):
			case "running":
				if(input(player_number,CONTROL_JUMP) and status != "jumping" and pressed_jump == 0 and hmap_inc_y <= 0)
					// salta
					status = "jumping";
					
					
					
					pressed_jump = 1;
					hmap_inc_y = jump_height;
					on_air = true;
				elseif(!input(player_number,INPUT_UI_RIGHT) and !input(player_number,INPUT_UI_LEFT))
					// frena
					status = "standing";
					
					
				
				else
					// corre
					char_speed = char_max_speed;
					if((hmap_collision_left == RGB(0,0,255) and hmap_inc_x < 0) or (hmap_collision_right == RGB(0,0,255) and hmap_inc_x > 0))
						char_speed -= 2;
						hmap_inc_y -= 1;
					end
					if(input(player_number,INPUT_UI_RIGHT))
						if(hmap_inc_x < char_speed)
							hmap_inc_x++;
						end
						flags = initial_flag;
					elseif(input(player_number,INPUT_UI_LEFT))
						flags = initial_flag + 1;
						if(hmap_inc_x > (char_speed*(-1)))
							hmap_inc_x--;
						end
					end
					
				end
			end
			case "standing":
				if(input(player_number,CONTROL_JUMP) and status != "jumping" and pressed_jump == 0 and hmap_inc_y <= 0)
					// cambia a jumping
					status = "jumping";
					
					
					
					pressed_jump = 1;
					hmap_inc_y = jump_height;
					on_air = true;
					if(hmap_inc_x > 0)
						hmap_inc_x--;
					elseif(hmap_inc_x < 0)
						hmap_inc_x++;
					end
				elseif(input(player_number,INPUT_UI_RIGHT) or input(player_number,INPUT_UI_LEFT)) 
					// cambia a running
					status = "running";
					
					
					
				else // standing
					if(hmap_inc_x > 0) // frenando para un lado
						hmap_inc_x--;
						
						
						
					elseif(hmap_inc_x < 0) // frenando pal otro
						hmap_inc_x++;
						
						
					end
					if(hmap_collision_bottom == RGB(0,0,255))
						hmap_inc_y -= 1;
					end
				end
			end
			case "jumping":
			
				if(input(player_number,INPUT_UI_RIGHT) or input(player_number,INPUT_UI_LEFT))
					// controla direccion en el salto
					char_speed = char_max_speed;
					if(input(player_number,INPUT_UI_RIGHT))
						if(hmap_inc_x < char_speed)
							hmap_inc_x++;
						end
						flags = initial_flag;
					elseif(input(player_number,INPUT_UI_LEFT))
						flags = initial_flag + 1;
						if(hmap_inc_x > (char_speed*(-1)))
							hmap_inc_x--;
						end
					end
				end
				
				// codigo para rebotar en la pared
				if ((input(player_number,INPUT_UI_RIGHT) and hmap_collision_right
				  or input(player_number,INPUT_UI_LEFT) and hmap_collision_left) and !hmap_collision_top)
				
					// frenando contra la pared
					if (hmap_inc_y>2)
						hmap_inc_y -= 2; 
					end
					
					
					
					if (input(player_number,INPUT_UI_RIGHT))
						flags = initial_flag + 1;
					else
						flags = initial_flag ;
					end
					
					if (input(player_number,CONTROL_JUMP) and !pressed_jump)
						pressed_jump = true;						
						
						if (input(player_number,INPUT_UI_RIGHT))
							//saltamos para la izquierda
							hmap_inc_x = -char_speed * 1.5;
						
						else
							//saltamos para la izquierda
						
							hmap_inc_x = char_speed * 1.5;
						end
						
						hmap_inc_y = jump_height;
					end
				
				end
				
				
				
				if(hmap_collision_bottom != 0)
					// toca el suelo
					on_air = false;
					hmap_inc_y = 0;
					status = "standing";			
				
				end
				

			end
			
		end

		frame;
	end
	onexit:
		unload_map(0,graph);
End

