/* 

BennuGD JoyPad Test

Created By: Nicholas Wolf

Purpose: For showing you what belongs to what on the joypad so that you can use it in the code 

*made with the XBOX360 controller in mind

*/

#ifndef __VERSION__
    import "mod_sdlevthandler";
    import "mod_pathfind";
#else
    import "mod_blendop";
    import "mod_text";
    import "mod_grproc";
    import "mod_video";
    import "mod_map";
    import "mod_screen";
    import "mod_path";
    import "mod_rand";
    import "mod_say";
    import "mod_mouse";
    import "mod_scroll";
    import "mod_math";
#endif

IMPORT "mod_debug";
IMPORT "mod_dir";
IMPORT "mod_file";
IMPORT "mod_key";
IMPORT "mod_draw";
IMPORT "mod_string";
IMPORT "mod_proc";
IMPORT "mod_wm";
IMPORT "mod_sys";
IMPORT "mod_joy";
IMPORT "mod_sound";
IMPORT "mod_time";
IMPORT "mod_timers";
IMPORT "mod_cd"

global
    //Joy buttons
        joy_but_0 = 0;
        joy_but_1 = 1;
        joy_but_2 = 2;
        joy_but_3 = 3;
        joy_but_4 = 4;
        joy_but_5 = 5;
        joy_but_6 = 6;
        joy_but_7 = 7;
        joy_but_8 = 8;
        joy_but_9 = 9;
        joy_but_10 = 10;
        joy_but_11 = 11;
        joy_but_12 = 12;
        
        hats[7][22];

process main()
begin
say("Joypads present: "+joy_number());
    loop
        //The buttons
            if(joy_getbutton(0,joy_but_0))
                say("This button is assigned to button 0");
            end
            if(joy_getbutton(0,joy_but_1))
                say("This button is assigned to button 1");
            end
            if(joy_getbutton(0,joy_but_2))
                say("This button is assigned to button 2");
            end
            if(joy_getbutton(0,joy_but_3))
                say("This button is assigned to button 3");
            end
            if(joy_getbutton(0,joy_but_4))
                say("This button is assigned to button 4");
            end
            if(joy_getbutton(0,joy_but_5))
                say("This button is assigned to button 5");
            end
            if(joy_getbutton(0,joy_but_6))
                say("This button is assigned to button 6");
            end
            if(joy_getbutton(0,joy_but_7))
                say("This button is assigned to button 7");
            end
            if(joy_getbutton(0,joy_but_8))
                say("This button is assigned to button 8");
            end
            if(joy_getbutton(0,joy_but_9))
                say("This button is assigned to button 9");
            end
            if(joy_getbutton(0,joy_but_10))
                say("This button is assigned to button 10");
            end
            if(joy_getbutton(0,joy_but_11))
                say("This button is assigned to button 11");
            end
            if(joy_getbutton(0,joy_but_12))
                say("This button is assigned to button 12");
            end
        //The Axis's
            if(joy_getaxis(0,0)<-10000);
                say("axis is going left");
            end
            if(joy_getaxis(0,0)>10000);
                say("axis is going right");
            end
            if(joy_getaxis(0,1)>10000);
                say("axis is going down");
            end
            if(joy_getaxis(0,1)<-10000);
                say("axis is going up");
            end
            if(joy_getaxis(0,2)<-10000);
                say("axis 2 is going up");
            end
            if(joy_getaxis(0,2)>10000);
                say("axis 2 is going down");
            end
            if(joy_getaxis(0,3)>10000);
                say("axis 2 is going down");
            end
            if(joy_getaxis(0,3)<-10000);
                say("axis 2 is going up");
            end
            if(joy_getaxis(0,4)<-10000);
                say("axis 3 is going left");
            end
            if(joy_getaxis(0,4)>10000);
                say("axis 3 is going right");
            end
        //The Hats
            hats[y][x]=JOY_GETHAT(y,x);
            if(hats[y][x]!=0)
                say("hats: "+hats[y][x]);
            end
        
        IF (key(_esc) or exit_status)
            exit("goodbye!",0);
        END
    frame; end
end
